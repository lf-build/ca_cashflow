﻿﻿using LendFoundry.Foundation.Services;
using LendFoundry.Foundation.Logging; 
using LendFoundry.Foundation.Client; 
using LendFoundry.Security.Tokens; 
using System; 
using Microsoft.Extensions.DependencyInjection; 
namespace CapitalAlliance.Cashflow.Client {
    public class CashflowClientFactory:ICashflowClientFactory {
        [Obsolete("Need to use the overloaded with Uri")]
        public CashflowClientFactory(IServiceProvider provider, string endpoint, int port) {
            Provider = provider; 
            Uri = new UriBuilder("http", endpoint, port).Uri; 
        }
  public CashflowClientFactory(IServiceProvider provider, Uri uri = null) {
            Provider = provider; 
            Uri = uri; 
        }

        private IServiceProvider Provider {get; set; }
        private Uri Uri {get; }

        public ICashflowService Create(ITokenReader reader) {
            var uri = Uri; 
            if (uri == null) {
                var logger = Provider.GetService < ILoggerFactory > ().Create(NullLogContext.Instance); 
                uri = Provider.GetRequiredService < IDependencyServiceUriResolverFactory > ().Create(reader, logger).Get("CASHFLOW"); 
            }
            var client = Provider.GetServiceClient(reader, uri); 
            return new CashflowClient(client); 
        }
    }
}