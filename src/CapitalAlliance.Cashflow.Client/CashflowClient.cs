﻿﻿using System.Threading.Tasks;
using LendFoundry.Foundation.Services;
using LendFoundry.Foundation.Client;
using RestSharp;
using System.Collections.Generic;
using System;

namespace CapitalAlliance.Cashflow.Client
{
    public class CashflowClient : ICashflowService
    {
        public CashflowClient(IServiceClient client)
        {
            Client = client;
        }

        private IServiceClient Client { get; }

        public async Task<string> GetBankName(string routingNumber)
        {
            var request = new RestRequest("bank/by/{routingNumber}", Method.GET);
            request.AddUrlSegment("routingNumber", routingNumber);
            return await Client.ExecuteAsync<string>(request);
        }

       public async Task<IAddAccountResponse> AddBankAccount(string entityType, string entityId, IBankAccountRequest BankAccount)
        {
            var request = new RestRequest("{entitytype}/{entityId}/add/account", Method.POST);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddJsonBody(BankAccount);
            return await Client.ExecuteAsync<AddAccountResponse>(request);
        }

        public async Task AddCashFlowData(string entityType, string entityId, ICashflowRequest CashflowRequest)
        {
            var request = new RestRequest("{entitytype}/{entityId}/add/cashflow", Method.POST);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddJsonBody(CashflowRequest);
             await Client.ExecuteAsync<object>(request);
        }    
        

        public Task<bool> ExtractAccountsAndTransactionsHandler(string entityType, string entityId, object data)
        {
            throw new NotImplementedException();
        }

        public async Task<string> AddBankTransaction(string entityType, string entityId, IBankTransactionRequest BankTransaction)
        {
            var request = new RestRequest("{entitytype}/{entityId}/add/transaction", Method.POST);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddJsonBody(BankTransaction);
            return await Client.ExecuteAsync<string>(request);
        }

        public async Task<bool> AddAccountPreference(string entityType, string entityId, IAccountPreferenceRequest accountPreference)
        {
            var request = new RestRequest("{entitytype}/{entityId}/add/account/preference", Method.POST);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddJsonBody(accountPreference);
            return await Client.ExecuteAsync<bool>(request);
        }

        public Task<IBankAccount> GetBankAccountDetails(string entityType, string entityId, string AccountId)
        {
            throw new NotImplementedException();
        }

        public Task<bool> CalculateManunalCashFlowEventHandler(string entityType, string entityId, object data)
        {
            throw new NotImplementedException();
        }

        public async Task<IAccountTypeResponse> GetAccountByType(string entityType, string entityId, IAccountTypeRequest request)
        {
            var requestAccount = new RestRequest("{entitytype}/{entityId}", Method.POST);
            requestAccount.AddUrlSegment("entitytype", entityType);
            requestAccount.AddUrlSegment("entityId", entityId);
            requestAccount.AddJsonBody(request);
            return await Client.ExecuteAsync<AccountTypeResponse>(requestAccount);
        }

        public async Task<bool> CalculateRunningBalance(string entityType, string entityId, object data)
        {
            var request = new RestRequest("{entitytype}/{entityId}/calculate/runningbalance", Method.POST);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddJsonBody(data);
            return await Client.ExecuteAsync<bool>(request);
        }

        public async Task<string> AddBankTransaction(string entityType, string entityId, IBulkTransactionRequest BankTransaction)
        {
            var request = new RestRequest("{entitytype}/{entityId}/add/transaction/bulk", Method.POST);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddJsonBody(BankTransaction);
            return await Client.ExecuteAsync<string>(request);
        }

        public async Task<ITransactionResponse> GetAccountTransaction(string entityType, string entityId, string accountId)
        {
            var request = new RestRequest("{entitytype}/{entityId}/transaction/{accountId}", Method.GET);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddUrlSegment("accountId", accountId);
            return await Client.ExecuteAsync<TransactionResponse>(request);
        }

        public async Task<bool> CalculateRunningBalanceAll(string entityType, string entityId)
        {
            var request = new RestRequest("{entitytype}/{entityId}/calculate/runningbalance/all", Method.GET);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityId", entityId);
            return await Client.ExecuteAsync<bool>(request);
        }

        public async Task<IAddAccountBulkResponse> AddBankAccountBulk(string entityType, string entityId, IBulkAccountRequest BankAccount)
        {
            var request = new RestRequest("{entitytype}/{entityId}/add/account/bulk", Method.POST);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddJsonBody(BankAccount);
            return await Client.ExecuteAsync<AddAccountBulkResponse>(request);
        }

        public async Task<string> GenerateAndSaveSingleCashflowReport(string entityType, string entityId, string accountNumber, CashflowReportType reportType, string month = null, string year = null)
        {
            var request = new RestRequest("{entitytype}/{entityId}/report/{accountNumber}/{reportType}/{month?}/{year?}", Method.GET);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddUrlSegment("accountNumber", accountNumber);
            request.AddUrlSegment("reportType", reportType.ToString());
            request.AddUrlSegment("month", month);
            request.AddUrlSegment("year", year);
            return await Client.ExecuteAsync<string>(request);
        }
        
        
        public async Task<string>DeleteTransactionByAccountId(string entityType, string entityId, string accountId)
        {
            var request = new RestRequest("{entitytype}/{entityId}/delete/transaction/by/account/{accountId}", Method.DELETE);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddUrlSegment("accountId", accountId);
            return await Client.ExecuteAsync<string>(request);
        }
        public async Task <bool> VerifyTransactionsCount(string entityType,string entityId, string accountId, int totalFetchedTransaction)
        {
            var request = new RestRequest("{entitytype}/{entityId}/verify/transaction/by/account/{accountId}/{totalFetchedTransaction}", Method.GET);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddUrlSegment("accountId", accountId);
            request.AddUrlSegment("totalFetchedTransaction", totalFetchedTransaction);
            return await Client.ExecuteAsync<bool>(request);
        }
    }
}