using LendFoundry.Security.Tokens; 
using Microsoft.Extensions.DependencyInjection; 
using LendFoundry.Foundation.Client;
using System; 


namespace CapitalAlliance.Cashflow.Client {
    public static class CashflowClientExtensions {
        [Obsolete("Need to use the overloaded with Uri")]
        public static IServiceCollection AddCashflowService(this IServiceCollection services, string endpoint, int port) {
            services.AddTransient < ICashflowClientFactory > (p => new CashflowClientFactory(p, endpoint, port)); 
            services.AddTransient(p => p.GetService < ICashflowClientFactory > ().Create(p.GetService < ITokenReader > ())); 
            return services; 
        }
    }
}