using LendFoundry.Security.Tokens;

namespace CapitalAlliance.Cashflow.Client
{
    public interface ICashflowClientFactory
    {
        ICashflowService Create(ITokenReader reader);
    }
}