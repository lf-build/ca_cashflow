﻿using System.Collections.Generic;

namespace CapitalAlliance.Cashflow
{
    public interface ICashflowConfiguration
    {
         string BanklookupUrl { get; set; }
         string Source { get; set; }
        List<EventConfiguration> events { get; set; }
        CashflowReportConfig CashflowReportConfig { get; set; }
    }
}