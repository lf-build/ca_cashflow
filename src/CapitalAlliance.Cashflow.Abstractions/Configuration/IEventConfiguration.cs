﻿namespace CapitalAlliance.Cashflow
{
    public interface IEventConfiguration
    {
        string EntityId { get; set; }
        string Response { get; set; }
        string EntityType { get; set; }
        string Name { get; set; }
        string MethodToExecute { get; set; }
        string CompletionEventName { get; set; }
    }
}