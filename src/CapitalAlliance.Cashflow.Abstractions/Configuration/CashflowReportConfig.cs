﻿namespace CapitalAlliance.Cashflow
{
    public class CashflowReportConfig
    {
        public string Category { get; set; }
        public string Logo { get; set; }
        public ReportAttribute Consolidated { get; set; }
        public ReportAttribute MonthlySummary { get; set; }
        public string NotAvailableAttributeText { get; set; }
    }   
}