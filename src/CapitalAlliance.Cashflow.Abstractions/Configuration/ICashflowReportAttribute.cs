﻿namespace CapitalAlliance.Cashflow
{
    public interface ICashflowReportAttribute
    {
        bool IsActive { get; set; }
        bool Transactions { get; set; }
        bool RecurringTransactions { get; set; }
    }
}