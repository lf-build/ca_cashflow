﻿using System.Collections.Generic;

namespace CapitalAlliance.Cashflow
{
    public class CashflowConfiguration: ICashflowConfiguration
    { 
        public string BanklookupUrl { get; set; }
        public string Source { get; set; }
        public List<EventConfiguration> events { get; set; }
        public CashflowReportConfig CashflowReportConfig { get; set; }
    }
}