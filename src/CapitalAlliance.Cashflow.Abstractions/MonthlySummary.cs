﻿using System.Collections.Generic;

namespace CapitalAlliance.Cashflow
{
    public class MonthlySummary: IMonthlySummary
    {
        public MonthlySummary() { }
        public MonthlySummary(Dictionary<string, string> request)
        {
            Name = request["Name"];
            Year = request["Year"];
            MonthlyAccountsSummary = request;
        }
        public string Name { get; set; }
        public string Year { get; set; }
        public Dictionary<string, string> MonthlyAccountsSummary { get; set; }
    }
}
