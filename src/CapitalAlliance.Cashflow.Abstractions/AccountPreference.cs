﻿namespace CapitalAlliance.Cashflow
{
    public class AccountPreference : IAccountPreference
    {
        public AccountPreference() { }
        public AccountPreference(IAccountPreferenceRequest request)
        {
            AccountID = request.AccountID;
            if(request.AccountType=="funding")
            {
                IsFundingAccount = true;
            }
            else if(request.AccountType=="cashflow")
            {
                IsCashflowAccount = true;
            }
        }
        public string AccountID { get; set; }
        public bool? IsCashflowAccount { get; set; }
        public bool? IsFundingAccount { get; set; }
    }
}