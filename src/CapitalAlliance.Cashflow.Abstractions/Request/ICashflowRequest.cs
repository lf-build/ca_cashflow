﻿using System.Collections.Generic;

namespace CapitalAlliance.Cashflow
{
    public interface ICashflowRequest
    {
        string AccountHeader { get; set; }
        string AccountID { get; set; }
        string AccountNumber { get; set; }
        string InstitutionName { get; set; }       
        string AccountType { get; set; }
        ICashflowDetails CashFlow { get; set; }
         bool HasSubReports { get; set; }
         string Source { get; set; }
    }
}