﻿namespace CapitalAlliance.Cashflow
{
    public class AccountPreferenceRequest : IAccountPreferenceRequest
    {
        public string AccountID { get; set; }
        public string AccountType { get; set; }
    }
}