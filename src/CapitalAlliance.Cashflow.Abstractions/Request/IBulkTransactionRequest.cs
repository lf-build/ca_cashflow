﻿using System.Collections.Generic;

namespace CapitalAlliance.Cashflow
{
    public interface IBulkTransactionRequest
    {
        List<ITransaction> bankTransaction { get; set; }
    }
}