﻿using System.Collections.Generic;

namespace CapitalAlliance.Cashflow
{
    public class BankTransactionRequest : IBankTransactionRequest
    {
        public string TransactionId { get; set; }
        public string AccountId { get; set; }
        public string ProviderAccountId { get; set; }
        public double Amount { get; set; }
        public string TransactionDate { get; set; }
        public string Description { get; set; }
        public string Pending { get; set; }
        public string CategoryId { get; set; }
        public IList<string> Categories { get; set; }
        public string Meta { get; set; }
    }
}