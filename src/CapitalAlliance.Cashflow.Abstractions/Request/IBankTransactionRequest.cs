﻿using System.Collections.Generic;

namespace CapitalAlliance.Cashflow
{
    public interface IBankTransactionRequest
    {
        string TransactionId { get; set; }
        string AccountId { get; set; }
        string ProviderAccountId { get; set; }
        double Amount { get; set; }
        string TransactionDate { get; set; }
        string Description { get; set; }
        string Pending { get; set; }
        string CategoryId { get; set; } 
        IList<string> Categories { get; set; }
        string Meta { get; set; }
    }
}