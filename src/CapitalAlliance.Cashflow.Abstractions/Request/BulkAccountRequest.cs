﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace CapitalAlliance.Cashflow
{
    public class BulkAccountRequest : IBulkAccountRequest
    {
        [JsonConverter(typeof(InterfaceListConverter<IBankAccount, BankAccount>))]
        public List<IBankAccount> bankAccount { get; set; }
    }
}