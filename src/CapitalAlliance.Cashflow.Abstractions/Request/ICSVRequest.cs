﻿namespace CapitalAlliance.Cashflow
{
    public interface ICSVRequest 
    {
        string accountNumber { get; set; }
        string instituteName { get; set; }
        string accountType { get; set; }
        string fileContent { get; set; }
        string fileName { get; set; }
    }
}