﻿using System.Collections.Generic;

namespace CapitalAlliance.Cashflow
{
    public interface IBulkAccountRequest
    {
        List<IBankAccount> bankAccount { get; set; }
    }
}