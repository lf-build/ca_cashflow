﻿namespace CapitalAlliance.Cashflow
{
    public interface IAccountPreferenceRequest
    {
        string AccountID { get; set; }
        string AccountType { get; set; }
    }
}