﻿namespace CapitalAlliance.Cashflow
{
    public interface IAccountBulkResponse
    {
        string AccountId { get; set; }
        string ProviderAccountId { get; set; }
    }
}