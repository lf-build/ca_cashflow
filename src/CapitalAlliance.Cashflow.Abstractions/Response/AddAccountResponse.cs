﻿
namespace CapitalAlliance.Cashflow
{
    public class AddAccountResponse : IAddAccountResponse
    {
        public string AccountId { get; set; }
    }
}