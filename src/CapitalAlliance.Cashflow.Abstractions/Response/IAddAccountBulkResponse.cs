﻿
using System.Collections.Generic;

namespace CapitalAlliance.Cashflow
{
    public interface IAddAccountBulkResponse
    {        
        List<IAccountBulkResponse> bankAccounts { get; set; }
    }
   
}