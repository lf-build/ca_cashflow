﻿
namespace CapitalAlliance.Cashflow
{
    public class AccountBulkResponse : IAccountBulkResponse
    {
        public AccountBulkResponse() { }
        public AccountBulkResponse(string accountid, string provideraccountid)
        {
            AccountId = accountid;
            ProviderAccountId = provideraccountid;
        }
        public string AccountId { get; set; }
        public string ProviderAccountId { get; set; }
    }
}