﻿using System.Collections.Generic;

namespace CapitalAlliance.Cashflow
{
    public interface IAccountTypeResponse
    {
        List<IBankAccount> accounts { get; set; }
    }
}