﻿using System.Collections.Generic;

namespace CapitalAlliance.Cashflow
{
    public interface ITransactionResponse
    {
        List<ITransaction> transaction { get; set; }
    }
}