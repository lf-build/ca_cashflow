﻿
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace CapitalAlliance.Cashflow
{
    public class AddAccountBulkResponse : IAddAccountBulkResponse
    {
        [JsonConverter(typeof(InterfaceListConverter<IAccountBulkResponse, AccountBulkResponse>))]
        public List<IAccountBulkResponse> bankAccounts { get; set; }
    }
}