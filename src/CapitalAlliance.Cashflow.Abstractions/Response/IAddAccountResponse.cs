﻿
namespace CapitalAlliance.Cashflow
{
    public interface IAddAccountResponse
    {
        string AccountId { get; set; }
    }
}