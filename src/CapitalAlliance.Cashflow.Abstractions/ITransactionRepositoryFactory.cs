﻿using LendFoundry.Security.Tokens;

namespace CapitalAlliance.Cashflow
{
    public interface ITransactionRepositoryFactory
    {
        ITransactionRepository Create(ITokenReader reader);
    }
}