﻿namespace CapitalAlliance.Cashflow
{
    public enum CashflowReportType
    {
        Both = 0,
        Summary = 1,
        Monthly=2
    }
}
