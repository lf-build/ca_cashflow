﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace CapitalAlliance.Cashflow
{
    public interface ITransactionRepository
    {         
        Task<string> AddTransaction(ITransaction request);
        Task<List<ITransaction>> GetAllBankTransaction(string entityType, string entityId,string accountId);
        Task<int> GetAllBankTransactionCount(string entityType, string entityId,string accountId);
        Task<string> AddTransaction(List<ITransaction> request);
        Task<string> DeleteTransactionByAccountId(string entityType, string entityId, string accountId);
    }
}