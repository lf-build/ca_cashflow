﻿using System.Collections.Generic;

namespace CapitalAlliance.Cashflow
{
    public interface IAccountsAndTransactions
    {
        List<BankAccount> Accounts { get; set; }
        List<Transaction> Transactions { get; set; }

    }
}