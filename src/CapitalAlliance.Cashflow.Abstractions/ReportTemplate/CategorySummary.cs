﻿namespace CapitalAlliance.Cashflow
{
    public class CategorySummary
    {
        public int TransactionCount { get; set; }
        public string CategoryName { get; set; }
        public double TransactionTotal { get; set; }
        public string CategoryId { get; set; }
        public string CustomCategoryId { get; set; }
        public int LastMonthTransactionCount { get; set; }
        public double LastMonthTransactionTotal { get; set; }
    }
}
