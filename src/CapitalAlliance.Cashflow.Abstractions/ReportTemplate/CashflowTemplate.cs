﻿namespace CapitalAlliance.Cashflow
{
    public class CashflowTemplate
    {
        public string AccountNumber { get; set; }
        public string AccountID { get; set; }
        public string InstitutionName { get; set; }
        public string AccountType { get; set; }
        public CashFlow CashFlow { get; set; }
        public string OfficialName { get; set; }
        public string NameOnAccount { get; set; }
    } 
}
