﻿using System.Collections.Generic;


namespace CapitalAlliance.Cashflow
{
    public class CashFlow
    {
        public List<MonthlyCashFlow> MonthlyCashFlows { get; set; }
        public TransactionSummary TransactionSummary { get; set; }
        public List<CategorySummary> CategorySummary { get; set; }
        public List<ReportTransaction> TransactionList { get; set; }
        public List<RecurringTransaction> RecurringList { get; set; }
    }
}
