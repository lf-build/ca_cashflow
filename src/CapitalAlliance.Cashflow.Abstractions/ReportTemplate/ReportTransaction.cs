﻿using System.Collections.Generic;


namespace CapitalAlliance.Cashflow
{
    public class ReportTransaction
    {
        public string Date { get; set; }
        public double Amount { get; set; }
        public string Description { get; set; }
        public string TransactionType { get; set; }
        public double RunningBalance { get; set; }
        public string Month { get; set; }
        public int Year { get; set; }
    }
}
