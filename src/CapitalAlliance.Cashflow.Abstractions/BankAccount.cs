﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using System;

namespace CapitalAlliance.Cashflow
{
    public class BankAccount : Aggregate, IBankAccount
    {
        public BankAccount() { }
        public BankAccount(IBankAccountRequest request)
        {
            RoutingNumber = request.RoutingNumber;
            BankName = request.BankName;
            AccountNumber = request.AccountNumber;
            CurrentBalance = request.CurrentBalance;
            AvailableBalance = request.AvailableBalance;
            AccountType = request.AccountType;
            Source = request.Source;
            NameOnAccount = request.NameOnAccount;
            if (!string.IsNullOrEmpty(request.BalanceAsOfDate))
            {
                BalanceAsOfDate = new TimeBucket(DateTimeOffset.Parse(request.BalanceAsOfDate));
            }
            Id = request.AccountId;
            ProviderAccountId = request.ProviderAccountId;
            IsCashflowAccount = request.IsCashflowAccount;
            IsFundingAccount = request.IsFundingAccount;
            OfficialAccountName = request.OfficialAccountName;
            if (!string.IsNullOrEmpty(request.AccessToken))
            {
                AccessToken = request.AccessToken;
            }
        }
        public string EntityId { get; set; }
        public string EntityType { get; set; }
        public string RoutingNumber { get; set; }
        public string BankName { get; set; }
        public string ProviderAccountId { get; set; }
        public string AccountNumber { get; set; }
        public double? CurrentBalance { get; set; }
        public double? AvailableBalance { get; set; }
        public string AccountType { get; set; }
        public string Source { get; set; }
        public TimeBucket BalanceAsOfDate { get; set; }
        public TimeBucket CreatedOn { get; set; }
        public TimeBucket UpdatedOn { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public string NameOnAccount { get; set; }
        public bool IsCashflowAccount { get; set; }
        public bool IsFundingAccount { get; set; }
        public string OfficialAccountName { get; set; }
        public string AccessToken { get; set; }
    }
}