﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace CapitalAlliance.Cashflow.Persistence
{
    public interface IAccountRepository
    {         
        Task<string> AddBankAccount(IBankAccount request);
        Task<bool> UpdateAccountPreference(IAccountPreference request);
        Task<IBankAccount> GetAccountDetails(string entityType, string entityId,string id);
        Task<List<IBankAccount>> GetAllBankAccounts(string entityType, string entityId);
        Task<IBankAccount> GetCashflowAccount(string entityType, string entityId);
        Task<IBankAccount> GetFundingAccount(string entityType, string entityId);
        Task<bool> ResetAccountPreference(string entityType, string entityId, AccountType accountType);
        Task<List<IBankAccount>> AddBankAccount(string entityType, string entityId, List<IBankAccount> request);
    }
}