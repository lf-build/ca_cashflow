﻿using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Date;
using System.Collections.Generic;
using System;

namespace CapitalAlliance.Cashflow
{
    public interface ITransaction : IAggregate
    {
        string EntityId { get; set; }
        string EntityType { get; set; }
        string AccountId { get; set; }
        string ProviderAccountId { get; set; }
        double Amount { get; set; }
        string TransactionDate { get; set; }
        string Description { get; set; }
        string Pending { get; set; }
        string CategoryId { get; set; }
        IList<string> Categories { get; set; }
        string Meta { get; set; }
        TimeBucket CreatedOn { get; set; }
        TimeBucket UpdatedOn { get; set; }
        double RunningBalance { get; set; }
        TimeBucket TransactionOn { get; set; }

    }
}