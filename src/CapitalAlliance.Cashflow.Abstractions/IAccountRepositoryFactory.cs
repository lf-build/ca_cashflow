﻿using LendFoundry.Security.Tokens;

namespace CapitalAlliance.Cashflow.Persistence
{
    public interface IAccountRepositoryFactory
    {
        IAccountRepository Create(ITokenReader reader);
    }
}