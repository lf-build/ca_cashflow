﻿using System.Xml.Serialization;

namespace CapitalAlliance.Cashflow
{

    [XmlRoot(ElementName = "Table", Namespace = "http://www.webserviceX.NET")]
        public class Table
        {
            [XmlElement(ElementName = "RoutingNumber", Namespace = "http://www.webserviceX.NET")]
            public string RoutingNumber { get; set; }
            [XmlElement(ElementName = "BankName", Namespace = "http://www.webserviceX.NET")]
            public string BankName { get; set; }
            [XmlElement(ElementName = "Address", Namespace = "http://www.webserviceX.NET")]
            public string Address { get; set; }
            [XmlElement(ElementName = "City", Namespace = "http://www.webserviceX.NET")]
            public string City { get; set; }
            [XmlElement(ElementName = "State", Namespace = "http://www.webserviceX.NET")]
            public string State { get; set; }
            [XmlElement(ElementName = "ZipCode", Namespace = "http://www.webserviceX.NET")]
            public string ZipCode { get; set; }
            [XmlElement(ElementName = "PhoneNumber", Namespace = "http://www.webserviceX.NET")]
            public string PhoneNumber { get; set; }
        }

        [XmlRoot(ElementName = "NewDataSet", Namespace = "http://www.webserviceX.NET")]
        public class NewDataSet
        {
            [XmlElement(ElementName = "Table", Namespace = "http://www.webserviceX.NET")]
            public Table Table { get; set; }
        }

        [XmlRoot(ElementName = "string", Namespace = "http://www.webserviceX.NET")]
        public class BankDetails
        {
            [XmlElement(ElementName = "NewDataSet", Namespace = "http://www.webserviceX.NET")]
            public NewDataSet NewDataSet { get; set; }
            [XmlAttribute(AttributeName = "xmlns")]
            public string Xmlns { get; set; }
        }


}
