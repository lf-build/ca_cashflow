﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace CapitalAlliance.Cashflow
{
    public interface ICashflowService
    {
        Task<string> GetBankName(string routingNumber);
        Task<IAddAccountResponse> AddBankAccount(string entityType, string entityId, IBankAccountRequest BankAccount);
        Task<string> AddBankTransaction(string entityType, string entityId, IBankTransactionRequest BankTransaction);
        Task AddCashFlowData(string entityType, string entityId, ICashflowRequest CashflowRequest);
        Task<bool> AddAccountPreference(string entityType, string entityId, IAccountPreferenceRequest accountPreference);
        Task<IBankAccount> GetBankAccountDetails(string entityType, string entityId, string AccountId);
        Task<bool> ExtractAccountsAndTransactionsHandler(string entityType, string entityId, object request);
        Task<bool> CalculateManunalCashFlowEventHandler(string entityType, string entityId, object request);
        Task<IAccountTypeResponse> GetAccountByType(string entityType, string entityId, IAccountTypeRequest request);
        Task<bool> CalculateRunningBalance(string entityType, string entityId, object data);
        Task<string> AddBankTransaction(string entityType, string entityId, IBulkTransactionRequest BankTransaction);

        Task<ITransactionResponse> GetAccountTransaction(string entityType, string entityId, string accountId);
        Task<bool> CalculateRunningBalanceAll(string entityType, string entityId);
        Task<IAddAccountBulkResponse> AddBankAccountBulk(string entityType, string entityId, IBulkAccountRequest BankAccount);
        Task<string> GenerateAndSaveSingleCashflowReport(string entityType, string entityId, string accountNumber, CashflowReportType reportType, string month = null, string year = null);
        Task<string> DeleteTransactionByAccountId(string entityType, string entityId, string accountId);
        Task <bool> VerifyTransactionsCount(string entityType,string entityId, string accountId, int totalFetchedTransaction);
    }
}