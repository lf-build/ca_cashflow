﻿using LendFoundry.Foundation.Services.Settings;
using System;

namespace CapitalAlliance.Cashflow
{
    public class Settings
    {
        private const string Prefix = "CASHFLOW";

        public static ServiceSettings EventHub { get; } = new ServiceSettings($"{Prefix}_EVENTHUB", "eventhub");

        public static ServiceSettings Configuration { get; } = new ServiceSettings($"{Prefix}_CONFIGURATION", "configuration");

        public static ServiceSettings Tenant { get; } = new ServiceSettings($"{Prefix}_TENANT", "tenant");
        
        public static ServiceSettings DataAttribute { get; } = new ServiceSettings($"{Prefix}_DATAATTRIBUTE_HOST", "data-attributes", $"{Prefix}_DATAATTRIBUTE_PORT");   

        public static DatabaseSettings Mongo { get; } = new DatabaseSettings($"{Prefix}_MONGO_CONNECTION", "mongodb://mongo", $"{Prefix}_MONGO_DATABASE", "cashflow");

        public static string ServiceName => Environment.GetEnvironmentVariable($"{Prefix}_TOKEN_ISSUER") ?? "cashflow";

        public static string Nats => Environment.GetEnvironmentVariable($"{Prefix}_NATS_URL") ?? "nats://nats:4222";
        public static ServiceSettings DecisionEngine { get; } = new ServiceSettings($"{Prefix}_DECISION_ENGINE", "decision-engine");
        public static ServiceSettings ApplicationDocument { get; } = new ServiceSettings($"{Prefix}_APPLICATIONDOCUMENT_HOST", "application-document", $"{Prefix}_APPLICATIONDOCUMENT_PORT");
        public static ServiceSettings DocumentGenerator { get; } = new ServiceSettings($"{Prefix}_DOCUMENTGENERATOR_HOST", "document-generator", $"{Prefix}_DOCUMENTGENERATOR_PORT");
        public static ServiceSettings ProductRule { get; } = new ServiceSettings($"{Prefix}_PRODUCTRULE_HOST", "ProductRule", $"{Prefix}_PRODUCTRULE_PORT");
    }
}