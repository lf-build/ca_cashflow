﻿using System;
using System.Collections.Generic;
using System.Linq;
namespace CapitalAlliance.Cashflow
{   

    public class CashflowDetails : ICashflowDetails
    {
        public CashflowDetails() { }
        public CashflowDetails(ICashflowDetails request)
        {
            MonthlyCashFlows = new List<Dictionary<string, string>>();
            if (request.MonthlyCashFlows != null && request.MonthlyCashFlows.Count > 0)
            {
                foreach (var cf in request.MonthlyCashFlows)
                {
                    if (!string.IsNullOrEmpty(cf["Name"]))
                    {
                        MonthlyCashFlows = request.MonthlyCashFlows;
                    }
                }                
            }           
            var objMonthlySummary = request.MonthlySummary == null ? new Dictionary<string, string>() : request.MonthlySummary;
            if (objMonthlySummary != null && objMonthlySummary.Count > 0)
            {
                if (!string.IsNullOrEmpty(objMonthlySummary["Name"]))
                {
                    MonthlyCashFlows.Add(objMonthlySummary);
                }
            }
            TransactionSummary = request.TransactionSummary == null ? new Dictionary<string, string>() : request.TransactionSummary;
            if (TransactionSummary != null && TransactionSummary.Count > 0)
            {
                var TempMonthlyCashFlows = new List<Dictionary<string, string>>();
                DateTime dtStart = DateTime.MinValue;
                DateTime dtEnd = DateTime.MinValue;
                if (!string.IsNullOrEmpty(TransactionSummary["StartDate"]))
                {
                    dtStart = Convert.ToDateTime(TransactionSummary["StartDate"]);
                }
                if (!string.IsNullOrEmpty(TransactionSummary["EndDate"]))
                {
                    dtEnd = Convert.ToDateTime(TransactionSummary["EndDate"]);
                }
                if (dtStart != DateTime.MinValue && dtEnd != DateTime.MinValue)
                {                    
                    while (dtStart <= dtEnd)
                    {
                        var objSummary = new Dictionary<string, string>();
                        objSummary.Add("Name", dtStart.ToString("MMMM"));
                        objSummary.Add("Year", dtStart.ToString("yyyy"));                        
                        dtStart = dtStart.AddMonths(1);
                        if (MonthlyCashFlows != null && MonthlyCashFlows.Count > 0)
                        {
                            if(!MonthlyCashFlows.Any(mcfs=> mcfs["Name"] == objSummary["Name"] && mcfs["Year"] == objSummary["Year"]))
                            {
                                TempMonthlyCashFlows.Add(objSummary);
                            }                           
                        }
                        else
                        {
                            TempMonthlyCashFlows.Add(objSummary);
                        }                    
                    }
                }
                if(TempMonthlyCashFlows!=null && TempMonthlyCashFlows.Count>0)
                {
                    MonthlyCashFlows.AddRange(TempMonthlyCashFlows);
                }
            }
            CategorySummary = request.CategorySummary == null ? new List<Dictionary<string, string>>() : request.CategorySummary;
            TransactionList = request.TransactionList == null ? new List<Dictionary<string, string>>() : request.TransactionList;
            RecurringList = request.RecurringList == null ? new List<Dictionary<string, string>>() : request.RecurringList;
            MCARecurringList = request.MCARecurringList == null ? new List<Dictionary<string, string>>() : request.MCARecurringList;
        }
        public List<Dictionary<string, string>> MonthlyCashFlows { get; set; }
        public Dictionary<string, string> TransactionSummary { get; set; }
        public List<Dictionary<string, string>> CategorySummary { get; set; }
        public List<Dictionary<string, string>> TransactionList { get; set; }
        public List<Dictionary<string, string>> RecurringList { get; set; }
        public List<Dictionary<string, string>> MCARecurringList { get; set; }
        public Dictionary<string, string> MonthlySummary { get; set; }
    }
}