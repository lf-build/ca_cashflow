﻿using System.Collections.Generic;

namespace CapitalAlliance.Cashflow
{
    public interface ICashflowDetails
    {
        Dictionary<string, string> MonthlySummary { get; set; }
        List<Dictionary<string, string>> MonthlyCashFlows { get; set; }
      
        Dictionary<string, string> TransactionSummary { get; set; }
        List<Dictionary<string,string>> CategorySummary { get; set; }
        List<Dictionary<string, string>> TransactionList { get; set; }
        List<Dictionary<string, string>> RecurringList { get; set; }
        List<Dictionary<string, string>> MCARecurringList { get; set; }
    }
}