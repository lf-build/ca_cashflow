﻿using LendFoundry.Application.Document.Client;
using CapitalAlliance.Cashflow.Persistence;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.Configuration;
using LendFoundry.DataAttributes.Client;
using LendFoundry.DocumentGenerator.Client;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.ProductRule;
using LendFoundry.ProductRule.Client;
using LendFoundry.Security.Tokens;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace CapitalAlliance.Cashflow
{
    public class CashflowServiceFactory : ICashflowServiceFactory
    {
        public CashflowServiceFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public ICashflowService Create(ITokenReader reader, ITokenHandler handler)
        {
            var loggerFactory = Provider.GetService<ILoggerFactory>();
            var logger = loggerFactory.Create(NullLogContext.Instance);

            var eventHubFactory = Provider.GetService<IEventHubClientFactory>();
            var eventHub = eventHubFactory.Create(reader);

            var configurationServiceFactory = Provider.GetService<IConfigurationServiceFactory>();
            var cashflowConfigurationService = configurationServiceFactory.Create<CashflowConfiguration>(Settings.ServiceName, reader);
            var cashflowConfiguration = cashflowConfigurationService.Get();


            var tenantTimeFactory = Provider.GetService<ITenantTimeFactory>();
            var tenantTime = tenantTimeFactory.Create(configurationServiceFactory, reader);

            var dataAttributesClientFactory = Provider.GetService<IDataAttributesClientFactory>();
            var dataAttributesService = dataAttributesClientFactory.Create(reader);

            var accountRepositoryFactory = Provider.GetService<IAccountRepositoryFactory>();
            var accountRepository = accountRepositoryFactory.Create(reader);

            var transactionRepositoryFactory = Provider.GetService<ITransactionRepositoryFactory>();
            var transactionRepository = transactionRepositoryFactory.Create(reader);

            var decisionEngineClientFactory = Provider.GetService<IDecisionEngineClientFactory>();
            var decisionEngineService = decisionEngineClientFactory.Create(reader);

            var applicationDocumentServiceClientFactory = Provider.GetService<IApplicationDocumentServiceClientFactory>();
            var applicationDocumentService = applicationDocumentServiceClientFactory.Create(reader);

            var documentGeneratorServiceFactory = Provider.GetService<IDocumentGeneratorServiceFactory>();
            var documentGeneratorService = documentGeneratorServiceFactory.Create(reader);

            var productRuleFactory = Provider.GetService<IProductRuleServiceClientFactory>();
            var productRuleEngine = productRuleFactory.Create(reader);

            return new CashflowService(logger, eventHub, tenantTime, dataAttributesService,
                cashflowConfiguration, handler,reader, accountRepository, transactionRepository, 
                decisionEngineService, applicationDocumentService, documentGeneratorService, productRuleEngine);
        }       
    }
}