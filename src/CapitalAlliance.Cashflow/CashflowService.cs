﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LendFoundry.Application.Document;
using CapitalAlliance.Cashflow.Persistence;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.DataAttributes;
using LendFoundry.DocumentGenerator;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.ProductRule;
using LendFoundry.Security.Tokens;
using LendFoundry.TemplateManager;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace CapitalAlliance.Cashflow {
    public class CashflowService : ICashflowService {
        #region Constructor

        public CashflowService
            (
                ILogger logger,
                IEventHubClient eventHubClient,
                ITenantTime tenantTime,
                IDataAttributesEngine dataAttributesService,
                ICashflowConfiguration cashflowConfigurationService,
                ITokenHandler tokenParser,
                ITokenReader tokenReader,
                IAccountRepository accountRepository,
                ITransactionRepository transactionRepository,
                IDecisionEngineService decisionEngineService,
                IApplicationDocumentService applicationDocumentService,
                IDocumentGeneratorService documentGeneratorService,
                IProductRuleService productRuleService
            ) {
                Logger = logger;
                EventHub = eventHubClient;
                TenantTime = tenantTime;
                DataAttributesService = dataAttributesService;
                CashflowConfigurationService = cashflowConfigurationService;
                TokenParser = tokenParser;
                TokenReader = tokenReader;
                AccountRepository = accountRepository;
                DecisionEngineService = decisionEngineService;
                TransactionRepository = transactionRepository;
                ApplicationDocumentService = applicationDocumentService;
                DocumentGeneratorService = documentGeneratorService;
                ProductRuleService = productRuleService;
            }
        #endregion

        #region Private Variables

        private IApplicationDocumentService ApplicationDocumentService { get; set; }
        private IDocumentGeneratorService DocumentGeneratorService { get; set; }
        private IAccountRepository AccountRepository { get; }
        private ITransactionRepository TransactionRepository { get; }
        private ILogger Logger { get; }
        private ITokenHandler TokenParser { get; }
        private ITokenReader TokenReader { get; }

        private IEventHubClient EventHub { get; }

        private ITenantTime TenantTime { get; }

        private ICashflowConfiguration CashflowConfigurationService { get; }

        private IDataAttributesEngine DataAttributesService { get; }
        private IDecisionEngineService DecisionEngineService { get; }
        private IProductRuleService ProductRuleService { get; set; }
        #endregion

        #region Add Bank Account
        public async Task<IAddAccountResponse> AddBankAccount (string entityType, string entityId, IBankAccountRequest BankAccount) {
            try {
                Logger.Info ("Started Execution for AddBankAccount at:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);

                var request = new BankAccount (BankAccount);
                Logger.Info ("Completed Execution for AddBankAccount at:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);
                var response = await AddBankAccount (entityType, entityId, request, true);
                var totalAccounts = await AccountRepository.GetAllBankAccounts (entityType, entityId);
                if (totalAccounts != null && totalAccounts.Count == 1) {
                    await EventHub.Publish ("BankLinked", new {
                        EntityId = entityId,
                            EntityType = entityType,
                            Response = request.AccountNumber,
                            Request = entityId,
                            ReferenceNumber = Guid.NewGuid ().ToString ("N")
                    });
                }
                return response;
            } catch (Exception exception) {
                Logger.Error ("Error While Processing AddBankAccount Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Exception" + exception.Message);
                throw;
            }
        }
        #endregion

        #region Add Bank Account Bulk
        public async Task<IAddAccountBulkResponse> AddBankAccountBulk (string entityType, string entityId, IBulkAccountRequest BankAccount) {
            IAddAccountBulkResponse result = new AddAccountBulkResponse ();
            result.bankAccounts = new List<IAccountBulkResponse> ();
            if (string.IsNullOrWhiteSpace (entityId))
                throw new ArgumentNullException (nameof (entityId));

            if (BankAccount == null)
                throw new ArgumentNullException (nameof (BankAccount));

            if (BankAccount.bankAccount == null)
                throw new ArgumentNullException (nameof (BankAccount.bankAccount), "Bank account list is not available");

            try {
                Logger.Info ("Started Execution for AddBankAccount bulk at:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);
                var accounts = await AccountRepository.AddBankAccount (entityType, entityId, BankAccount.bankAccount);
                foreach (var item in accounts) {
                    result.bankAccounts.Add (new AccountBulkResponse (item.Id, item.ProviderAccountId));
                }
            } catch (Exception ex) {
                Logger.Error ("Error While Processing AddBankAccount bulk at:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Exception :" + ex.Message + ex.StackTrace);
                throw;
            }
            Logger.Info ("Ended Execution for AddBankAccount bulk at:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);
            return result;
        }
        #endregion

        #region Add Bank Account
        private async Task<IAddAccountResponse> AddBankAccount (string entityType, string entityId, IBankAccount request, bool raiseCashflowEvent = false) {
            if (string.IsNullOrWhiteSpace (entityId))
                throw new ArgumentNullException (nameof (entityId));

            if (request == null)
                throw new ArgumentNullException (nameof (request));

            if (string.IsNullOrWhiteSpace (request.Source)) {
                request.Source = CashflowConfigurationService.Source;
            }
            if (request.BalanceAsOfDate == null) {
                request.BalanceAsOfDate = new TimeBucket (TenantTime.Now.UtcDateTime);
            }

            request.EntityId = entityId;
            request.EntityType = entityType;
            if (string.IsNullOrEmpty (request.Id)) {
                request.CreatedOn = new TimeBucket (TenantTime.Now.UtcDateTime);
                request.CreatedBy = EnsureCurrentUser ();
            }
            request.UpdatedOn = new TimeBucket (TenantTime.Now.UtcDateTime);
            request.UpdatedBy = EnsureCurrentUser ();
            var accountId = await AccountRepository.AddBankAccount (request);
            if (raiseCashflowEvent) {
                //edit account
                var accounts = await AccountRepository.GetAccountDetails (entityType, entityId, request.Id);

                await CalculatePlaidCashflow (entityType, entityId, accounts, ProductRuleService, DataAttributesService);
            }
            var response = new AddAccountResponse ();
            response.AccountId = accountId;

            if (!string.IsNullOrEmpty (request.Id)) {
                await EventHub.Publish ("CashflowUpdated", new {
                    EntityId = entityId,
                        EntityType = entityType,
                        Response = request.AccountNumber,
                        Request = entityId,
                        ReferenceNumber = Guid.NewGuid ().ToString ("N")
                });
            }

            return response;
        }
        #endregion

        #region Add Bank Transaction
        public async Task<string> AddBankTransaction (string entityType, string entityId, IBulkTransactionRequest BankTransaction) {
            string result = string.Empty;
            if (string.IsNullOrWhiteSpace (entityId))
                throw new ArgumentNullException (nameof (entityId));

            if (BankTransaction == null)
                throw new ArgumentNullException (nameof (BankTransaction));

            if (BankTransaction.bankTransaction == null)
                throw new ArgumentNullException (nameof (BankTransaction.bankTransaction), "Bank transaction list is not available");

            try {
                Logger.Info ("Started Execution for AddBankTransaction bulk at:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);

                await TransactionRepository.AddTransaction (BankTransaction.bankTransaction);
                result = "success";
            } catch (Exception ex) {
                Logger.Error ("Error While Processing AddBankTransaction bulk at:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Exception :" + ex.Message + ex.StackTrace);

                throw;
            }
            Logger.Info ("Ended Execution for AddBankTransaction bulk at:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);
            return result;
        }
        #endregion

        #region Add Bank Transaction
        public async Task<string> AddBankTransaction (string entityType, string entityId, IBankTransactionRequest BankTransaction) {
            var request = new Transaction (BankTransaction);
            return await AddBankTransaction (entityType, entityId, request);
        }
        #endregion

        #region Add Bank Transaction
        private async Task<string> AddBankTransaction (string entityType, string entityId, ITransaction request) {
            if (string.IsNullOrWhiteSpace (entityId))
                throw new ArgumentNullException (nameof (entityId));

            if (request == null)
                throw new ArgumentNullException (nameof (request));

            request.EntityId = entityId;
            request.EntityType = entityType;
            if (string.IsNullOrEmpty (request.Id)) {
                request.CreatedOn = new TimeBucket (TenantTime.Now.UtcDateTime);
            } else {
                request.UpdatedOn = new TimeBucket (TenantTime.Now.UtcDateTime);
            }
            return await TransactionRepository.AddTransaction (request);
        }
        #endregion

        #region Ensure CurrentUser
        private string EnsureCurrentUser () {
            if (TokenParser != null) {
                var token = TokenParser.Parse (TokenReader.Read ());
                var username = token?.Subject;
                if (string.IsNullOrWhiteSpace (token?.Subject))
                    throw new ArgumentException ("User is not authorized");
                return username;
            }
            return null;
        }
        #endregion

        #region Add CashFlow Data
        public async Task AddCashFlowData (string entityType, string entityId, ICashflowRequest CashflowRequest) {
            try {
                Logger.Info ("Started Execution for AddCashFlowData at:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);

                if (string.IsNullOrWhiteSpace (entityId))
                    throw new ArgumentNullException (nameof (entityId));

                if (CashflowRequest == null)
                    throw new ArgumentNullException (nameof (CashflowRequest));

                var data = new CashflowRequest (CashflowRequest);
                await DataAttributesService.SetAttribute (entityType, entityId, "plaidCashflow", data, data.AccountID);

                await EventHub.Publish ("CashflowUpdated", new {
                    EntityId = entityId,
                        EntityType = entityType,
                        Response = data.AccountNumber,
                        Request = entityId,
                        ReferenceNumber = Guid.NewGuid ().ToString ("N")
                });
                Logger.Info ("Completed Execution for AddCashFlowData at:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);

            } catch (Exception exception) {
                Logger.Error ("Error While Processing AddCashFlowData Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Exception" + exception.Message);

                throw;
            }
        }
        #endregion

        #region Get Bank Name
        public async Task<string> GetBankName (string routingNumber) {
            if (string.IsNullOrWhiteSpace (routingNumber))
                throw new ArgumentNullException (nameof (routingNumber));
            try {
                Logger.Info ("Started Execution for GetBankName at:" + TenantTime.Now);
                var restRequest = new RestRequest (Method.GET);
                var uri = new Uri (CashflowConfigurationService.BanklookupUrl);
                restRequest.AddParameter ("RoutingNumber", routingNumber, ParameterType.QueryString);
                var client = new RestClient (uri);
                var response = await client.ExecuteTaskAsync (restRequest);

                if (response == null)
                    throw new NotFoundException ("Bank name not available for provided RoutingNumber");

                if (response.StatusCode == System.Net.HttpStatusCode.OK) {
                    string xmalResponse = System.Net.WebUtility.HtmlDecode (response.Content);

                    var result = XmlDeserialization.Deserialize<BankDetails> (xmalResponse);

                    if (result == null)
                        throw new NullReferenceException (nameof (result));

                    if (result.NewDataSet == null)
                        throw new NullReferenceException (nameof (result.NewDataSet));

                    if (result.NewDataSet.Table == null)
                        throw new NotFoundException ("Bank name not available for provided RoutingNumber");

                    Logger.Info ("Completed Execution for GetBankName at:" + TenantTime.Now);

                    return result.NewDataSet.Table.BankName.Trim ();
                } else {
                    return "fake_institution";
                }
            } catch (Exception exception) {
                Logger.Error ("Error While Processing AddCashFlowData Date & Time:" + TenantTime.Now + "Exception" + exception.Message);

                throw;
            }
        }
        #endregion

        #region Add Account Preference
        public async Task<bool> AddAccountPreference (string entityType, string entityId, IAccountPreferenceRequest accountPreference) {
            if (string.IsNullOrWhiteSpace (entityId))
                throw new ArgumentNullException (nameof (entityId));

            if (accountPreference == null)
                throw new ArgumentNullException (nameof (accountPreference));
            try {
                Logger.Info ("Started Execution for AddAccountPreference at:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);
                var data = new AccountPreference (accountPreference);
                if (data.IsCashflowAccount.HasValue && data.IsCashflowAccount.Value == true) {
                    await AccountRepository.ResetAccountPreference (entityType, entityId, AccountType.Cashflow);
                    await EventHub.Publish ("CashflowAccountSelected", new {
                        EntityId = entityId,
                            EntityType = entityType,
                            Response = new { dataAttributeName = accountPreference.AccountID },
                            Request = entityId,
                            ReferenceNumber = Guid.NewGuid ().ToString ("N")
                    });
                } else if (data.IsFundingAccount.HasValue && data.IsFundingAccount.Value == true) {
                    var account = await AccountRepository.GetAccountDetails (entityType, entityId, data.AccountID);
                    await DataAttributesService.SetAttribute (entityType, entityId, "fundingAccount", account);
                    await AccountRepository.ResetAccountPreference (entityType, entityId, AccountType.Funding);
                }
                Logger.Info ("Completed Execution for AddAccountPreference at:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);
                return await AccountRepository.UpdateAccountPreference (data);
            } catch (Exception exception) {
                Logger.Error ("Error While Processing AddAccountPreference Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Exception" + exception.Message);
                throw;
            }
        }
        #endregion

        #region Get Bank Account Details
        public async Task<IBankAccount> GetBankAccountDetails (string entityType, string entityId, string accountId) {
            if (string.IsNullOrWhiteSpace (entityId))
                throw new ArgumentNullException (nameof (entityId));
            return await AccountRepository.GetAccountDetails (entityType, entityId, accountId);
        }
        #endregion

        #region Private Methods

        private string RemoveSpace (string value) {
            return value.Trim ().Replace (" ", "");
        }

        #region Execute Request
        private T ExecuteRequest<T> (string response) where T : class {
            try {
                return JsonConvert.DeserializeObject<T> (response);
            } catch (Exception exception) {
                throw new Exception ("Unable to deserialize:" + response, exception);
            }
        }
        #endregion

        #endregion Private Methods

        #region Extract Accounts And Transactions Handler
        public async Task<bool> ExtractAccountsAndTransactionsHandler (string entityType, string entityId, object data) {
            bool result = false;
            try {
                Logger.Info ("Started Execution for ExtractAccountsAndTransactionsHandler at:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);
                if (String.IsNullOrWhiteSpace (entityId))
                    throw new ArgumentNullException (nameof (entityId));
                if (data == null)
                    throw new ArgumentNullException (nameof (data));

                var objAccount = JsonConvert.DeserializeObject<Dictionary<string, string>> (data.ToString ());

                var accountId = objAccount["AccountId"];
                var IsSelected = Convert.ToBoolean (objAccount["IsSelected"]);
                var lstAccounts = new List<string> ();
                lstAccounts.Add (accountId);

                var accounts = await AccountRepository.GetAccountDetails (entityType, entityId, accountId);

                await CalculatePlaidCashflow (entityType, entityId, accounts, ProductRuleService, DataAttributesService);

                #region Account Preference

                if (IsSelected == true) {
                    await DataAttributesService.SetAttribute (entityType, entityId, "fundingAccount", accounts);
                    await EventHub.Publish ("CashflowAccountSelected", new {
                        EntityId = entityId,
                            EntityType = entityType,
                            Response = new { dataAttributeName = accounts.Id },
                            Request = entityId,
                            ReferenceNumber = Guid.NewGuid ().ToString ("N")
                    });
                }
                #endregion

                #region PDF Creation

                if (CashflowConfigurationService.CashflowReportConfig.Consolidated.IsActive) {
                    GenerateAndSaveCashflowReport (entityType, entityId, lstAccounts,
                        DataAttributesService,
                        CashflowConfigurationService, DocumentGeneratorService, ApplicationDocumentService, Logger);
                }
                #endregion

                result = true;
            } catch (Exception ex) {
                Logger.Error ($"ExtractAccountsAndTransactionsHandler Error For : {entityId} , Error : {ex.Message + ex.StackTrace}");
            }
            Logger.Info ("Completed Execution for ExtractAccountsAndTransactionsHandler at:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);
            return result;
        }
        #endregion

        #region Calculate Plaid Cashflow
        private async Task CalculatePlaidCashflow (string entityType, string entityId, IBankAccount accounts,
            IProductRuleService productRuleService, IDataAttributesEngine dataAttributesService) {
            Logger.Info ("Started Execution for CalculatePlaidCashflow at:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);
            try {
                var productId = string.Empty;
                var objApplicationDataAttribute = await dataAttributesService.GetAttribute (entityType, entityId, "product");
                if (objApplicationDataAttribute != null) {
                    var lstApplicationResult = (JArray) objApplicationDataAttribute;
                    if (lstApplicationResult != null && lstApplicationResult.Count > 0) {
                        var json = (JObject) JsonConvert.DeserializeObject (lstApplicationResult[0] + "");

                        Dictionary<string, object> d = new Dictionary<string, object> (json.ToObject<IDictionary<string, object>> (), StringComparer.CurrentCultureIgnoreCase);

                        productId = d["ProductId"] + "";
                    }
                }
                var objRuleResult = await productRuleService.RunRule (entityType, entityId, productId, "PlaidCalculateCashFlow", new { Accounts = accounts });

                if (objRuleResult != null && objRuleResult.IntermediateData != null) {
                    var intermediateResult = JsonConvert.DeserializeObject<Dictionary<string, object>> (objRuleResult.IntermediateData.ToString ());
                    if (intermediateResult != null && intermediateResult.Any ()) {
                        foreach (var attribute in intermediateResult) {
                            await dataAttributesService.SetAttribute (entityType, entityId, "plaidCashflow", attribute.Value, attribute.Key);
                        }
                    }
                }

                await CalculateRunningBalance (entityType, entityId, accounts, TransactionRepository);

                Logger.Info ("Ended Execution for CalculatePlaidCashflow at:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);
            } catch (Exception ex) {
                Logger.Error ($"CalculatePlaidCashflow Error For : {entityId} , Error : {ex.Message + ex.StackTrace}");
            }
        }
        #endregion

        #region Save Account And Transaction
        private async Task SaveAccountAndTransaction (string entityType, string entityId, dynamic ruleResult,
            IDataAttributesEngine dataAttributesService,
            ICashflowConfiguration cashflowConfigurationService,
            IDocumentGeneratorService documentGeneratorService,
            IApplicationDocumentService applicationDocumentService,
            ILogger Logger,
            IProductRuleService productRuleService) {
            List<string> lstAccounts = new List<string> ();
            AccountsAndTransactions objInput = new AccountsAndTransactions ();
            objInput = ExecuteRequest<AccountsAndTransactions> (ruleResult + "");
            bool IsTransactionsAvailable = false;
            var allTransactions = new List<ITransaction> ();
            if (objInput.Transactions != null && objInput.Transactions.Count > 0) {
                allTransactions = new List<ITransaction> (objInput.Transactions.Cast<ITransaction> ());
                IsTransactionsAvailable = true;
            }
            if (objInput.Accounts != null && objInput.Accounts.Count > 0) {
                string selectedCashflowAccountId = string.Empty;
                foreach (var account in objInput.Accounts) {
                    var objAccountTransaction = new List<ITransaction> ();
                    var objResponse = await AddBankAccount (entityType, entityId, account, false);
                    var accountId = string.Empty;
                    if (objResponse != null) {
                        accountId = objResponse.AccountId;
                    }
                    lstAccounts.Add (accountId);
                    if (account.IsCashflowAccount == true) {
                        selectedCashflowAccountId = accountId;
                        await dataAttributesService.SetAttribute (entityType, entityId, "fundingAccount", account);
                    }
                    if (IsTransactionsAvailable) {
                        objAccountTransaction = allTransactions.Where (t => t.ProviderAccountId == account.ProviderAccountId).ToList ();
                        if (objAccountTransaction != null && objAccountTransaction.Count > 0) {
                            objAccountTransaction.ForEach (t => t.AccountId = accountId);
                        }
                        foreach (var transaction in objAccountTransaction) {
                            var TransactionDate_OffSet = Convert.ToDateTime (transaction.TransactionDate);
                            transaction.TransactionOn = new TimeBucket (TransactionDate_OffSet);
                            await AddBankTransaction (entityType, entityId, transaction);
                        }
                    }
                    await CalculatePlaidCashflow (entityType, entityId, account,
                        productRuleService, dataAttributesService);
                }

                if (!string.IsNullOrEmpty (selectedCashflowAccountId)) {
                    await EventHub.Publish ("CashflowAccountSelected", new {
                        EntityId = entityId,
                            EntityType = entityType,
                            Response = new { dataAttributeName = selectedCashflowAccountId },
                            Request = entityId,
                            ReferenceNumber = Guid.NewGuid ().ToString ("N")
                    });
                }

            }
            if (cashflowConfigurationService.CashflowReportConfig.Consolidated.IsActive) {
                GenerateAndSaveCashflowReport (entityType, entityId, lstAccounts,
                    dataAttributesService,
                    cashflowConfigurationService, documentGeneratorService, applicationDocumentService, Logger);
            }
        }
        #endregion

        #region Format Money
        private string FormatMoney (double amount) {
            string result = string.Empty;
            var usCulture = CultureInfo.CreateSpecificCulture ("en-US");
            var clonedNumbers = (NumberFormatInfo) usCulture.NumberFormat.Clone ();
            clonedNumbers.CurrencyNegativePattern = 2;
            result = amount.ToString ("C", clonedNumbers);
            return result;
        }
        #endregion

        #region Format Percentage
        private string FormatPercentage (double amount) {
            string result = string.Empty;
            result = amount.ToString () + "%";
            return result;
        }
        #endregion

        #region Generate And Save Cashflow Report
        private async Task GenerateAndSaveCashflowReport (string entityType, string entityId, List<string> data,
            IDataAttributesEngine dataAttributesService,
            ICashflowConfiguration cashflowConfigurationService,
            IDocumentGeneratorService documentGeneratorService,
            IApplicationDocumentService applicationDocumentService,
            ILogger Logger, CashflowReportType reportType = CashflowReportType.Both, string month = null, string year = null) {
            try {
                if (String.IsNullOrWhiteSpace (entityId))
                    throw new ArgumentNullException (nameof (entityId));
                if (data == null)
                    throw new ArgumentNullException (nameof (data));
                #region Cashflow Report Data

                var objApplicationDataAttribute = await dataAttributesService.GetAttribute (entityType, entityId, "application");
                if (objApplicationDataAttribute != null) {
                    var AccountHolderName = string.Empty;
                    var Email = string.Empty;
                    var lstApplicationResult = (JArray) objApplicationDataAttribute;
                    if (lstApplicationResult != null && lstApplicationResult.Count > 0) {
                        AccountHolderName = lstApplicationResult[0]["businessApplicantName"] + "";
                        Email = lstApplicationResult[0]["applicantWorkEmail"] + "";
                    }
                    List<CashflowTemplate> cashflowTmplateList = new List<CashflowTemplate> ();
                    foreach (var key in data) {
                        var name = key;
                        object objDataAttribute = null;
                        FaultRetry.RunWithAlwaysRetry (() => {
                            objDataAttribute = dataAttributesService.GetAttribute (entityType, entityId, "plaidCashflow", name).Result;
                            if (objDataAttribute == null)
                                throw new InvalidOperationException ($"{name} not found for EntityId : {entityId}");
                        });
                        if (objDataAttribute == null)
                            throw new InvalidOperationException ($"{name} not found for EntityId : {entityId}");
                        if (objDataAttribute != null) {
                            var lstCashFlowResult = (JArray) objDataAttribute;
                            var item = new CashflowTemplate ();
                            if (lstCashFlowResult != null && lstCashFlowResult.Count > 0) {
                                item = lstCashFlowResult[0].ToObject<CashflowTemplate> ();
                            }

                            cashflowTmplateList.Add (item);
                        }
                    }
                    #endregion
                    await CashflowReportHandler (entityType, entityId, cashflowTmplateList, AccountHolderName, Email,
                        dataAttributesService,
                        cashflowConfigurationService, documentGeneratorService, applicationDocumentService, Logger);
                }
            } catch (Exception ex) {
                Logger.Error ($"CashflowCalculationCompletedHandler Error For : {entityId} , Error : {ex.Message + ex.StackTrace}");
            }
        }
        #endregion

        #region Calculate Manunal CashFlow Event Handler
        public async Task<bool> CalculateManunalCashFlowEventHandler (string entityType, string entityId, object data) {
            if (string.IsNullOrWhiteSpace (entityId))
                throw new ArgumentNullException (nameof (entityId));
            if (data == null)
                throw new ArgumentNullException (nameof (data));
            bool result = false;
            try {
                var CSVCashflowRequest = ExecuteRequest<CSVRequest> (data.ToString ());
                if (CSVCashflowRequest.fileContent != null) {
                    byte[] fileBytes = Convert.FromBase64String (CSVCashflowRequest.fileContent);

                    //convert document bytes to JSON
                    string objCSVJson = Encoding.UTF8.GetString (fileBytes);

                    var allLines = objCSVJson.Split (new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
                    var header = allLines[0].Split (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                    var csvData = allLines.Skip (1)
                        .Select (l => l.Split (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                            .Select ((s, i) => new { s, i })
                            .ToDictionary (x => header[x.i], x => x.s));

                    var settings = new JsonSerializerSettings ();
                    settings.NullValueHandling = NullValueHandling.Ignore;
                    settings.DefaultValueHandling = DefaultValueHandling.Ignore;
                    settings.StringEscapeHandling = StringEscapeHandling.Default;

                    var objCSVResponse = JsonConvert.SerializeObject (csvData, settings);
                    CSVCashflowRequest.fileContent = "";

                    var lstBankStatement = ExecuteRequest<List<BankStatement>> (objCSVResponse);

                    var payload = new { data = lstBankStatement, details = CSVCashflowRequest };

                    var ruleResult = DecisionEngineService.Execute<dynamic> ("ExtractCSVData", new { payload = payload });
                    if (ruleResult != null) {
                        SaveAccountAndTransaction (entityType, entityId, ruleResult, DataAttributesService, CashflowConfigurationService,
                            DocumentGeneratorService, ApplicationDocumentService, Logger, ProductRuleService);
                    }
                    result = true;
                } else {
                    Logger.Error ($"CalculateManunalCashFlowEventHandler empty uploaded file content : {entityId}");
                }
            } catch (Exception ex) {
                Logger.Error ($"CalculateManunalCashFlowEventHandler Error For : {entityId} , Error : {ex.Message + ex.StackTrace}");
            }
            return result;
        }
        #endregion

        #region Get Account By Type
        public async Task<IAccountTypeResponse> GetAccountByType (string entityType, string entityId, IAccountTypeRequest request) {
            if (string.IsNullOrWhiteSpace (entityId))
                throw new ArgumentNullException (nameof (entityId));
            if (request == null)
                throw new ArgumentNullException (nameof (request));

            var lstResult = new AccountTypeResponse ();
            try {
                Logger.Info ("Started Execution for GetAccountByType at:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);

                lstResult.accounts = new List<IBankAccount> ();
                if (request.IsCashflowAccount.HasValue && request.IsCashflowAccount.Value == true) {
                    try {
                        var cashflowAccount = await AccountRepository.GetCashflowAccount (entityType, entityId);
                        if (cashflowAccount != null) {
                            lstResult.accounts.Add (cashflowAccount);
                        }
                    } catch { }
                }
                if (request.IsFundingAccount.HasValue && request.IsFundingAccount.Value == true) {
                    try {
                        var fundingAccount = await AccountRepository.GetFundingAccount (entityType, entityId);
                        if (fundingAccount != null) {
                            lstResult.accounts.Add (fundingAccount);
                        }
                    } catch { }
                }
            } catch (Exception exception) {
                Logger.Error ("Error While Processing GetAccountByType Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Exception" + exception.Message + exception.StackTrace);
            }
            Logger.Info ("Completed Execution for GetAccountByType at:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);
            return lstResult;
        }
        #endregion

        #region Calculate Running Balance

        public async Task<bool> CalculateRunningBalance (string entityType, string entityId, object data) {
            var accountId = string.Empty;
            var result = false;
            var transactions = new List<ITransaction> ();
            try {
                Logger.Info ("Started Execution for CalculateRunningBalance at : " + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);
                if (String.IsNullOrWhiteSpace (entityId))
                    throw new ArgumentNullException (nameof (entityId));
                if (data == null)
                    throw new ArgumentNullException (nameof (data));

                var objAccount = JsonConvert.DeserializeObject<Dictionary<string, string>> (data.ToString ());
                accountId = objAccount.FirstOrDefault ().Value;

                IBankAccount account = await AccountRepository.GetAccountDetails (entityType, entityId, accountId);
                await CalculateRunningBalance (entityType, entityId, account, TransactionRepository);
                result = true;
            } catch (Exception ex) {
                Logger.Error ($"CalculateRunningBalance Error For : {entityId} , AccountId : {accountId},  Error : {ex.Message + ex.StackTrace}");
            }

            return result;
        }

        private async Task<bool> CalculateRunningBalance (string entityType, string entityId, IBankAccount account,
            ITransactionRepository transactionRepository, bool updateDate = false) {
            var transactions = new List<ITransaction> ();
            try {
                Logger.Info ("Started Execution for CalculateRunningBalance at : " + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);
                if (String.IsNullOrWhiteSpace (entityId))
                    throw new ArgumentNullException (nameof (entityId));

                if (account == null)
                    throw new ArgumentNullException (nameof (account));
                if (account.CurrentBalance.HasValue) {
                    var endingBalance = account.CurrentBalance.Value;
                    var beginingBalance = endingBalance;

                    transactions = await transactionRepository.GetAllBankTransaction (entityType, entityId, account.Id);
                    if (transactions != null && transactions.Count > 0) {
                        if (updateDate == true) {
                            foreach (var item in transactions) {
                                var TransactionDate_OffSet = Convert.ToDateTime (item.TransactionDate);
                                item.TransactionOn = new TimeBucket (TransactionDate_OffSet);
                                await AddBankTransaction (entityType, entityId, item);
                            }
                            transactions = await transactionRepository.GetAllBankTransaction (entityType, entityId, account.Id);
                        }

                        if (account.Source.ToLower () == "plaid" || account.Source.ToLower () == "csv") {
                            var lstTemptransactions = new List<ITransaction> ();
                            lstTemptransactions = transactions.OrderByDescending (i => i.TransactionOn.Time).ToList ();
                            var EndDate = lstTemptransactions[0].TransactionDate;
                            var StartDate = lstTemptransactions[lstTemptransactions.Count - 1].TransactionDate;
                            var maxTransactionDate = Convert.ToDateTime (EndDate);
                            var currentMonth = maxTransactionDate.Month;
                            var currentYear = maxTransactionDate.Year;
                            var isFirstTransaction = true;
                            var dateOfLastTransaction = new TimeBucket ();
                            while (lstTemptransactions.Count > 0) {
                                var currentMonthTransactions = lstTemptransactions.Where (i => i.TransactionOn.Time.Month == currentMonth && i.TransactionOn.Time.Year == currentYear).ToList ();
                                if (currentMonthTransactions != null && currentMonthTransactions.Count > 0) {
                                    foreach (var trans in currentMonthTransactions) {
                                        var currentTransactionDate = trans.TransactionOn;
                                        var day = currentTransactionDate.Day;
                                        var calculateDailyEnding = isFirstTransaction ? isFirstTransaction : (dateOfLastTransaction.Time != currentTransactionDate.Time);
                                        if (calculateDailyEnding) {
                                            endingBalance = beginingBalance;
                                        }
                                        dateOfLastTransaction = currentTransactionDate;

                                        if (trans.Amount < 0) {
                                            beginingBalance -= Math.Abs (trans.Amount);
                                        } else {
                                            beginingBalance += trans.Amount;
                                        }
                                        isFirstTransaction = false;
                                        var objTransaction = transactions.FirstOrDefault (i => i.Id == trans.Id);
                                        objTransaction.RunningBalance = endingBalance;
                                        await transactionRepository.AddTransaction (objTransaction);
                                        lstTemptransactions.Remove (trans);
                                    }
                                }
                                currentMonth -= 1;
                                if (currentMonth < 0) {
                                    currentYear -= 1;
                                    currentMonth = 12;
                                }
                            }
                        }
                    }
                } else {
                    Logger.Info ($"No CurrentBalance found for : {entityId} , AccountId : {account.Id}");
                    return false;
                }
            } catch (Exception ex) {
                Logger.Error ($"CalculateRunningBalance Error For : {entityId} , AccountId : {account.Id},  Error : {ex.Message + ex.StackTrace}");
            }
            return true;
        }

        public async Task<bool> CalculateRunningBalanceAll (string entityType, string entityId) {
            var accountId = string.Empty;
            var result = false;
            var transactions = new List<ITransaction> ();
            try {
                Logger.Info ("Started Execution for CalculateRunningBalance at : " + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);
                if (String.IsNullOrWhiteSpace (entityId))
                    throw new ArgumentNullException (nameof (entityId));

                var lstAccounts = await AccountRepository.GetAllBankAccounts (entityType, entityId);
                if (lstAccounts != null && lstAccounts.Count > 0) {
                    foreach (var account in lstAccounts) {
                        await CalculateRunningBalance (entityType, entityId, account, TransactionRepository, true);
                    }
                }
            } catch (Exception ex) {
                Logger.Error ($"CalculateRunningBalance Error For : {entityId} , AccountId : {accountId},  Error : {ex.Message + ex.StackTrace}");
            }

            return result;
        }
        #endregion

        #region Get Transactions
        public async Task<ITransactionResponse> GetAccountTransaction (string entityType, string entityId, string accountId) {
            if (string.IsNullOrWhiteSpace (entityId))
                throw new ArgumentNullException (nameof (entityId));
            if (string.IsNullOrEmpty (accountId))
                throw new ArgumentNullException (nameof (accountId));

            var lstResult = new TransactionResponse ();
            try {
                Logger.Info ("Started Execution for GetAccountTransaction at:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);

                lstResult.transaction = new List<ITransaction> ();
                lstResult.transaction = await TransactionRepository.GetAllBankTransaction (entityType, entityId, accountId);
            } catch (Exception exception) {
                Logger.Error ("Error While Processing GetAccountTransaction Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Exception" + exception.Message);
            }
            Logger.Info ("Completed Execution for GetAccountTransaction at:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId);
            return lstResult;
        }
        #endregion

        #region Generate specific cashflow report
        public async Task<string> GenerateAndSaveSingleCashflowReport (string entityType, string entityId, string accountNumber, CashflowReportType reportType, string month = null, string year = null) {
            if (string.IsNullOrWhiteSpace (entityId))
                throw new ArgumentNullException (nameof (entityId));
            if (accountNumber == null)
                throw new ArgumentNullException (nameof (accountNumber));
            //if (reportType == CashflowReportType.Both)
            //    throw new ArgumentNullException(nameof(reportType));
            if (reportType.Equals (CashflowReportType.Monthly) && (month == null || year == null)) {
                if (month == null)
                    throw new ArgumentNullException (nameof (month), "Month of report is not available");
                if (year == null)
                    throw new ArgumentNullException (nameof (year), "Year of report is not available");
            }
            List<IBankAccount> accounts = await AccountRepository.GetAllBankAccounts (entityType, entityId);
            if (accounts == null || accounts.Count == 0) {
                throw new NotFoundException ($"No Accounts found for EntityId : {entityId}");
            }
            IBankAccount selectedAccount = accounts.FirstOrDefault (x => x.AccountNumber.Equals (accountNumber));
            if (selectedAccount == null) {
                throw new InvalidOperationException ($"No Account found with AccountNumber {accountNumber} for EntityId : {entityId}");
            }
            List<string> lstAccounts = new List<string> {
                selectedAccount.Id.ToString ()
            };

            #region Cashflow Report Data
            var objApplicationDataAttribute = await DataAttributesService.GetAttribute (entityType, entityId, "application");
            if (objApplicationDataAttribute == null)
                throw new InvalidOperationException ($"{accountNumber} not found for EntityId : {entityId}");
            var AccountHolderName = string.Empty;
            var Email = string.Empty;
            var lstApplicationResult = (JArray) objApplicationDataAttribute;
            if (lstApplicationResult != null && lstApplicationResult.Count > 0) {
                AccountHolderName = lstApplicationResult[0]["businessApplicantName"] + "";
                Email = lstApplicationResult[0]["applicantWorkEmail"] + "";
            }
            List<CashflowTemplate> cashflowTmplateList = new List<CashflowTemplate> ();
            var name = selectedAccount.Id.ToString ();
            object objDataAttribute = null;
            FaultRetry.RunWithAlwaysRetry (() => {
                objDataAttribute = DataAttributesService.GetAttribute (entityType, entityId, "plaidCashflow", name).Result;
                if (objDataAttribute == null)
                    throw new InvalidOperationException ($"{accountNumber} not found for EntityId : {entityId}");
            });
            if (objDataAttribute == null)
                throw new InvalidOperationException ($"{accountNumber} not found for EntityId : {entityId}");
            var lstCashFlowResult = (JArray) objDataAttribute;
            var item = new CashflowTemplate ();
            if (lstCashFlowResult != null && lstCashFlowResult.Count > 0) {
                item = lstCashFlowResult[0].ToObject<CashflowTemplate> ();
            }
            if (reportType.Equals (CashflowReportType.Monthly)) {
                int givenYear;
                try {
                    givenYear = Convert.ToInt32 (year);
                } catch (Exception e) {
                    throw new InvalidArgumentException ($"{year} is not valid for EntityId : {entityId}");
                }
                item.CashFlow.MonthlyCashFlows = item.CashFlow.MonthlyCashFlows.Where (x => x.Year == givenYear && x.Name.ToLower ().Equals (month.ToLower ())).ToList ();
                if (item.CashFlow.MonthlyCashFlows == null || item.CashFlow.MonthlyCashFlows.Count == 0) {
                    throw new InvalidOperationException ($"{month}-{year} not found Acc. No.{accountNumber} for EntityId : {entityId}");
                }
            }
            cashflowTmplateList.Add (item);
            #endregion
            await CashflowReportHandler (entityType, entityId, cashflowTmplateList, AccountHolderName, Email, DataAttributesService, CashflowConfigurationService, DocumentGeneratorService, ApplicationDocumentService, Logger, reportType, month, year);
            return string.Format ("Cashflow {0} PDF generated for EntityId:{1}", reportType, entityId);
        }
        #endregion

        #region Cashflow PDF Report Handler
        private async Task CashflowReportHandler (string entityType, string entityId, List<CashflowTemplate> data, string accountHolderName, string email,
            IDataAttributesEngine dataAttributesService,
            ICashflowConfiguration cashflowConfigurationService,
            IDocumentGeneratorService documentGeneratorService,
            IApplicationDocumentService applicationDocumentService,
            ILogger Logger, CashflowReportType reportType = CashflowReportType.Both, string month = null, string year = null) {
            foreach (CashflowTemplate item in data) {
                ICashflowDocument objCashflowReport = new CashflowDocument ();
                ICashflowDocument objMonthlyReport = new CashflowDocument ();
                if (item != null) {
                    objCashflowReport.Logo = cashflowConfigurationService.CashflowReportConfig.Logo;

                    #region Account Section

                    objCashflowReport.CashflowAccountSection = new CashflowAccountSection ();
                    objCashflowReport.CashflowAccountSection.AccountNumber = item.AccountNumber;
                    objCashflowReport.CashflowAccountSection.AccountHolderName = accountHolderName;
                    objCashflowReport.CashflowAccountSection.LoanNumber = entityId;
                    objCashflowReport.CashflowAccountSection.InstitutionName = item.InstitutionName;
                    objCashflowReport.CashflowAccountSection.AccountType = item.AccountType;
                    objCashflowReport.CashflowAccountSection.Email = email;
                    objCashflowReport.CashflowAccountSection.OfficialName = item.OfficialName;
                    objCashflowReport.CashflowAccountSection.NameOnAccount = item.NameOnAccount;

                    #endregion

                    if (item.CashFlow != null) {
                        if (reportType == CashflowReportType.Both || (reportType != CashflowReportType.Both && reportType.Equals (CashflowReportType.Summary))) {

                            #region Category Summary Section

                            if (item.CashFlow.CategorySummary != null && item.CashFlow.CategorySummary.Count > 0) {
                                objCashflowReport.CashflowCategorySummary = new List<CashflowCategorySummarySection> ();
                                foreach (var category in item.CashFlow.CategorySummary) {
                                    if (!string.IsNullOrEmpty (category.CategoryName)) {
                                        CashflowCategorySummarySection objSummary = new CashflowCategorySummarySection ();
                                        objSummary.CategoryId = category.CategoryId;
                                        objSummary.CategoryName = category.CategoryName;
                                        objSummary.CustomCategoryId = category.CustomCategoryId;
                                        objSummary.LastMonthTransactionCount = category.LastMonthTransactionCount + "";
                                        objSummary.LastMonthTransactionTotal = FormatMoney (category.LastMonthTransactionTotal);
                                        objSummary.TransactionCount = category.TransactionCount + "";
                                        objSummary.TransactionTotal = FormatMoney (category.TransactionTotal);
                                        objCashflowReport.CashflowCategorySummary.Add (objSummary);
                                    }
                                }
                                objCashflowReport.CashflowCategorySummary = objCashflowReport.CashflowCategorySummary.OrderBy (x => x.CustomCategoryId).ToList ();
                            }
                            #endregion

                            #region Transaction Summary Section

                            if (item.CashFlow.TransactionSummary != null) {
                                objCashflowReport.TransactionSummary = new TransactionSummarySection ();
                                objCashflowReport.TransactionSummary.AsOfDate = DateTime.UtcNow.ToString ("MM/dd/yyyy");
                                objCashflowReport.TransactionSummary.AverageDailyBalance = FormatMoney (item.CashFlow.TransactionSummary.AverageDailyBalance);
                                if (item.CashFlow.TransactionSummary.AvailableBalance.HasValue) {
                                    objCashflowReport.TransactionSummary.AvailableBalance = FormatMoney (item.CashFlow.TransactionSummary.AvailableBalance.Value);
                                } else {
                                    objCashflowReport.TransactionSummary.AvailableBalance = cashflowConfigurationService.CashflowReportConfig.NotAvailableAttributeText;
                                }
                                if (item.CashFlow.TransactionSummary.CurrentBalance.HasValue) {
                                    objCashflowReport.TransactionSummary.CurrentBalance = FormatMoney (item.CashFlow.TransactionSummary.CurrentBalance.Value);
                                } else {
                                    objCashflowReport.TransactionSummary.CurrentBalance = cashflowConfigurationService.CashflowReportConfig.NotAvailableAttributeText;
                                }
                                objCashflowReport.TransactionSummary.TotalCredits = FormatMoney (item.CashFlow.TransactionSummary.TotalCredits);
                                objCashflowReport.TransactionSummary.TotalDebits = FormatMoney (item.CashFlow.TransactionSummary.TotalDebits);
                                objCashflowReport.TransactionSummary.AverageBalanceLastMonth = FormatMoney (item.CashFlow.TransactionSummary.AverageBalanceLastMonth);
                                objCashflowReport.TransactionSummary.AverageDeposit = FormatMoney (item.CashFlow.TransactionSummary.AverageDeposit);
                                objCashflowReport.TransactionSummary.AverageWithdrawal = FormatMoney (item.CashFlow.TransactionSummary.AverageWithdrawal);
                                objCashflowReport.TransactionSummary.ChangeInDepositVolume = item.CashFlow.TransactionSummary.ChangeInDepositVolume + "";
                                objCashflowReport.TransactionSummary.StartDate = item.CashFlow.TransactionSummary.StartDate;
                                objCashflowReport.TransactionSummary.EndDate = item.CashFlow.TransactionSummary.EndDate;
                                objCashflowReport.TransactionSummary.NumberOfNegativeBalance = item.CashFlow.TransactionSummary.NumberOfNegativeBalance + "";
                                objCashflowReport.TransactionSummary.CountOfMonthlyStatement = item.CashFlow.TransactionSummary.CountOfMonthlyStatement + "";
                                objCashflowReport.TransactionSummary.TotalCreditsCount = item.CashFlow.TransactionSummary.TotalCreditsCount + "";
                                objCashflowReport.TransactionSummary.TotalDebitsCount = item.CashFlow.TransactionSummary.TotalDebitsCount + "";
                                objCashflowReport.TransactionSummary.AnnualCalculatedRevenue = FormatMoney (item.CashFlow.TransactionSummary.AnnualCalculatedRevenue);
                                objCashflowReport.TransactionSummary.CVOfDailyBalance = FormatPercentage (item.CashFlow.TransactionSummary.CVOfDailyBalance);
                                objCashflowReport.TransactionSummary.CVOfDailyDeposit = FormatPercentage (item.CashFlow.TransactionSummary.CVOfDailyDeposit);
                                objCashflowReport.TransactionSummary.MedianDailyBalance = FormatMoney (item.CashFlow.TransactionSummary.MedianDailyBalance);
                                objCashflowReport.TransactionSummary.MedianMonthlyIncome = FormatMoney (item.CashFlow.TransactionSummary.MedianMonthlyIncome);
                                objCashflowReport.TransactionSummary.MaxDaysBelow100Count = item.CashFlow.TransactionSummary.MaxDaysBelow100Count + "";
                                objCashflowReport.TransactionSummary.NumberOfNSF = item.CashFlow.TransactionSummary.NumberOfNSF + "";
                                objCashflowReport.TransactionSummary.OfficialName = item.CashFlow.TransactionSummary.OfficialName;
                                if (!string.IsNullOrEmpty (item.CashFlow.TransactionSummary.BrokerAGSText)) {
                                    objCashflowReport.TransactionSummary.BrokerAGS = FormatMoney (item.CashFlow.TransactionSummary.BrokerAGS) + "[" + item.CashFlow.TransactionSummary.BrokerAGSText + "]";
                                } else {
                                    objCashflowReport.TransactionSummary.BrokerAGS = FormatMoney (item.CashFlow.TransactionSummary.BrokerAGS);
                                }
                                objCashflowReport.TransactionSummary.CAPAGS = FormatMoney (item.CashFlow.TransactionSummary.CAPAGS);
                            }

                            #endregion
                        }

                        #region Get Transactions

                        var lstTransactionList = new List<ITransaction> ();
                        if (cashflowConfigurationService.CashflowReportConfig.Consolidated.Transactions == true ||
                            (cashflowConfigurationService.CashflowReportConfig.MonthlySummary.IsActive == true &&
                                cashflowConfigurationService.CashflowReportConfig.MonthlySummary.Transactions == true)) {
                            lstTransactionList = await TransactionRepository.GetAllBankTransaction (entityType, entityId, item.AccountID);
                        }
                        #endregion

                        if (cashflowConfigurationService.CashflowReportConfig.MonthlySummary.IsActive == true) {
                            if (reportType == CashflowReportType.Both || (reportType != CashflowReportType.Both && reportType.Equals (CashflowReportType.Monthly))) {
                                #region Monthly Summary Section
                                if (item.CashFlow.MonthlyCashFlows != null && item.CashFlow.MonthlyCashFlows.Count > 0) {
                                    objMonthlyReport.CashflowAccountSection = objCashflowReport.CashflowAccountSection;
                                    objMonthlyReport.Logo = objCashflowReport.Logo;

                                    objMonthlyReport.MonthlySummary = new MonthlyCashFlowSummarySection ();

                                    foreach (var monthSummary in item.CashFlow.MonthlyCashFlows) {
                                        #region Monthly Summary Section

                                        objMonthlyReport.MonthlySummary.AverageDailyBalance = FormatMoney (monthSummary.AverageDailyBalance);
                                        objMonthlyReport.MonthlySummary.BeginingBalance = FormatMoney (monthSummary.BeginingBalance);
                                        objMonthlyReport.MonthlySummary.DepositCount = monthSummary.DepositCount + "";
                                        objMonthlyReport.MonthlySummary.EndingBalance = FormatMoney (monthSummary.EndingBalance);
                                        objMonthlyReport.MonthlySummary.FirstTransactionDate = monthSummary.FirstTransactionDate;
                                        objMonthlyReport.MonthlySummary.EndTransactionDate = monthSummary.EndTransactionDate;
                                        objMonthlyReport.MonthlySummary.LoanPaymentAmount = FormatMoney (monthSummary.LoanPaymentAmount);
                                        objMonthlyReport.MonthlySummary.MaxDepositAmount = FormatMoney (monthSummary.MaxDepositAmount);
                                        objMonthlyReport.MonthlySummary.MaxWithdrawalAmount = FormatMoney (monthSummary.MaxWithdrawalAmount);
                                        objMonthlyReport.MonthlySummary.MinDepositAmount = FormatMoney (monthSummary.MinDepositAmount);
                                        objMonthlyReport.MonthlySummary.MinWithdrawalAmount = FormatMoney (monthSummary.MinWithdrawalAmount);
                                        objMonthlyReport.MonthlySummary.NSFAmount = FormatMoney (monthSummary.NSFAmount);
                                        objMonthlyReport.MonthlySummary.OverdraftAmount = FormatMoney (monthSummary.OverdraftAmount);
                                        objMonthlyReport.MonthlySummary.Name = monthSummary.Name;
                                        objMonthlyReport.MonthlySummary.NumberOfLoanPayment = monthSummary.NumberOfLoanPayment + "";
                                        objMonthlyReport.MonthlySummary.NumberOfNSF = monthSummary.NumberOfNSF + "";
                                        objMonthlyReport.MonthlySummary.NumberOfOverdraft = monthSummary.NumberOfOverdraft + "";
                                        objMonthlyReport.MonthlySummary.NumberOfNegativeBalance = monthSummary.NumberOfNegativeBalance + "";
                                        objMonthlyReport.MonthlySummary.NumberOfPayroll = monthSummary.NumberOfPayroll + "";
                                        objMonthlyReport.MonthlySummary.PayrollAmount = FormatMoney (monthSummary.PayrollAmount);
                                        objMonthlyReport.MonthlySummary.TotalDepositAmount = FormatMoney (monthSummary.TotalDepositAmount);
                                        objMonthlyReport.MonthlySummary.TotalWithdrawalAmount = FormatMoney (monthSummary.TotalWithdrawalAmount);
                                        objMonthlyReport.MonthlySummary.WithdrawalCount = monthSummary.WithdrawalCount + "";
                                        objMonthlyReport.MonthlySummary.Year = monthSummary.Year + "";

                                        #endregion

                                        if (cashflowConfigurationService.CashflowReportConfig.MonthlySummary.Transactions == true) {
                                            #region Transaction List
                                            if (lstTransactionList != null && lstTransactionList.Count > 0) {
                                                objMonthlyReport.TransactionList = new List<CashflowTransactionListSection> ();
                                                var objCurrentMonthTransactions = lstTransactionList.Where (t => t.TransactionOn.Time.Year == monthSummary.Year && CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName (t.TransactionOn.Time.Month) == monthSummary.Name).ToList ();
                                                if (objCurrentMonthTransactions != null && objCurrentMonthTransactions.Count > 0) {
                                                    objCurrentMonthTransactions = objCurrentMonthTransactions.OrderByDescending (i => i.TransactionOn.Time).ToList ();
                                                    foreach (var trans in objCurrentMonthTransactions) {
                                                        CashflowTransactionListSection objTrans = new CashflowTransactionListSection ();
                                                        objTrans.Amount = FormatMoney (trans.Amount * (-1));
                                                        objTrans.Description = trans.Description;
                                                        objTrans.Date = trans.TransactionOn.Time.ToString ("MM/dd/yyyy");
                                                        objTrans.TransactionType = trans.Amount > 0 ? "Debit" : "Credit";
                                                        objTrans.RunningBalance = FormatMoney (trans.RunningBalance);
                                                        objMonthlyReport.TransactionList.Add (objTrans);
                                                    }
                                                }
                                            }
                                            #endregion
                                        }
                                        #region Generate PDF
                                        if (monthSummary.PDFReportName != null) {
                                            try {
                                            var monthlyDocument = new LendFoundry.DocumentGenerator.Document {
                                            Data = objMonthlyReport,
                                            Format = DocumentFormat.Pdf,
                                            Name = monthSummary.PDFReportName,
                                            Version = "1.0"
                                                };
                                                //Logger.Info($"Document Generator Called For EntityId : {entityId} , AccountID : {item.AccountID}, Month : {objMonthlyReport.MonthlySummary.Name}, Year : {objMonthlyReport.MonthlySummary.Year}");
                                                var monthlyResult = await documentGeneratorService.Generate ("CashflowMonthlySummaryReport", "1.0", Format.Html, monthlyDocument);

                                                if (monthlyResult != null) {
                                                    //Logger.Info($"Application Document Called For EntityId : {entityId} , AccountID : {item.AccountID}, Month : {objMonthlyReport.MonthlySummary.Name}, Year : {objMonthlyReport.MonthlySummary.Year}");
                                                    await applicationDocumentService.Add (applicationNumber: entityId, category: cashflowConfigurationService.CashflowReportConfig.Category, fileBytes: monthlyResult.Content, fileName: monthlyDocument.Name, tags: null, metaData: null);
                                                }
                                            } catch (System.Exception ex) {
                                                Logger.Info ($"Document Generator Called Failed!!! EntityId : {entityId} , Category : {cashflowConfigurationService.CashflowReportConfig.Category}, AccountID : {item.AccountID}, Month : {objMonthlyReport.MonthlySummary.Name}, Year : {objMonthlyReport.MonthlySummary.Year} -Error Message: {ex}");
                                            }
                                        } else {
                                            Logger.Info ($"Account have not transaction EntityId : {entityId} , AccountID : {item.AccountID}");

                                        }
                                        #endregion
                                    }
                                }
                                #endregion
                            }
                        }
                        if (reportType == CashflowReportType.Both || (reportType != CashflowReportType.Both && reportType.Equals (CashflowReportType.Summary))) {
                            if (cashflowConfigurationService.CashflowReportConfig.Consolidated.Transactions == true) {
                                #region Transaction List                                       

                                if (lstTransactionList != null && lstTransactionList.Count > 0) {
                                    lstTransactionList = lstTransactionList.OrderByDescending (i => i.TransactionOn.Time).ToList ();
                                    objCashflowReport.TransactionList = new List<CashflowTransactionListSection> ();
                                    foreach (var trans in lstTransactionList) {
                                        CashflowTransactionListSection objTrans = new CashflowTransactionListSection ();
                                        objTrans.Amount = FormatMoney (trans.Amount  * (-1));
                                        objTrans.Description = trans.Description;
                                        objTrans.Date = trans.TransactionOn.Time.ToString ("MM/dd/yyyy");
                                        objTrans.TransactionType = trans.Amount > 0 ? "Debit" : "Credit";
                                        objTrans.RunningBalance = FormatMoney (trans.RunningBalance);
                                        objCashflowReport.TransactionList.Add (objTrans);
                                    }
                                }
                                #endregion
                            }

                            if (cashflowConfigurationService.CashflowReportConfig.Consolidated.RecurringTransactions == true) {
                                #region Recurring Transactions
                                objCashflowReport.RecurringTransaction = new List<RecurringTransactionSection> ();
                                if (item.CashFlow.RecurringList != null && item.CashFlow.RecurringList.Count > 0) {
                                    foreach (var rt in item.CashFlow.RecurringList) {
                                        if (!string.IsNullOrEmpty (rt.Name)) {
                                            var objRecurring = new RecurringTransactionSection ();
                                            objRecurring.Amount = FormatMoney (rt.Amount  * (-1));
                                            objRecurring.RoudingAmount = FormatMoney (rt.RoudingAmount * (-1));
                                            objRecurring.Name = rt.Name;
                                            objRecurring.TotalTransactions = rt.TotalTransactions;
                                            objRecurring.Transactions = new List<CashflowTransactionListSection> ();
                                            foreach (var trans in rt.Transactions) {
                                                CashflowTransactionListSection objTrans = new CashflowTransactionListSection ();
                                                objTrans.Amount = FormatMoney (trans.Amount * (-1));
                                                objTrans.Description = trans.Description;
                                                objTrans.Date = trans.Date;
                                                objTrans.TransactionType = trans.TransactionType;
                                                objRecurring.Transactions.Add (objTrans);
                                            }
                                            objRecurring.ConfidenceLevel = rt.ConfidenceLevel;
                                            objCashflowReport.RecurringTransaction.Add (objRecurring);
                                        }
                                    }
                                }
                                #endregion
                            }
                            if (item.CashFlow.TransactionSummary.PDFReportName != null) {
                                var document = new LendFoundry.DocumentGenerator.Document {
                                Data = objCashflowReport,
                                Format = DocumentFormat.Pdf,
                                Name = item.CashFlow.TransactionSummary.PDFReportName,
                                Version = "1.0"
                                };
                                try {
                                    Logger.Info ($"Started Document Generator Request For EntityId : {entityId} , AccountID : {item.AccountID} PDF Name: {document.Name}");
                                    var documentResult = await documentGeneratorService.Generate ("CashflowSummaryReport", "1.0", Format.Html, document);
                                    Logger.Info ($"Ended Document Generator Request EntityId : {entityId} , AccountID : {item.AccountID}");
                                    if (documentResult != null) {
                                        Logger.Info ($"Started Application Document Request For EntityId : {entityId} , AccountID : {item.AccountID}");
                                        await applicationDocumentService.Add (applicationNumber: entityId, category: cashflowConfigurationService.CashflowReportConfig.Category, fileBytes: documentResult.Content, fileName: document.Name, tags: null, metaData: null);
                                        Logger.Info ($"Ended Application Document Request For EntityId : {entityId} , AccountID : {item.AccountID}");
                                    }
                                } catch (Exception ex) {
                                    Logger.Error ($"Cashflow PDF Generator Error For ReportName : {item.CashFlow.TransactionSummary.PDFReportName}, and App# : {entityId} , AccountID : {item.AccountID}, Error : {ex.Message + ex.StackTrace}");
                                }
                            }
                        }
                    }
                }
            }

        }
        #endregion 
        #region VerifyTransaction
        public async Task<bool> VerifyTransactionsCount (string entityType, string entityId, string accountId, int totalFetchedTransaction) {
            await Task.Yield ();
            Logger.Info ("Started Execution for Verify Transaction at:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + "for AccountId:" + accountId + "total Fetched Transaction is:" + totalFetchedTransaction);

            var transactionlist = await TransactionRepository.GetAllBankTransactionCount (entityType, entityId, accountId);
            if (transactionlist > 0) {
                if (transactionlist < totalFetchedTransaction) {
                    Logger.Info ("Verify Transaction failed " + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + "for AccountId:" + accountId);
                    Logger.Info ("Transaction Difference " + "totalFetched V/S dbsaved Transaction" + totalFetchedTransaction + "/" + transactionlist);
                    await TransactionRepository.DeleteTransactionByAccountId (entityType, entityId, accountId);
                    return false;
                }
                return true;
            }
            return false;
        }

        #endregion
        #region DeleteTransaction
        public async Task<string> DeleteTransactionByAccountId (string entityType, string entityId, string accountId) {
            string result = String.Empty;
            if (string.IsNullOrWhiteSpace (entityId))
                throw new ArgumentNullException (nameof (entityId));
            if (accountId == null)
                throw new ArgumentNullException (nameof (accountId));
            try {
                Logger.Info ("Started Execution for Delete Transaction at:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + "for AccountId:" + accountId);
                result = await TransactionRepository.DeleteTransactionByAccountId (entityType, entityId, accountId);
            } catch (Exception ex) {
                Logger.Error ("Error While Processing Deleting Transaction at:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "for AccountId:" + accountId + "Exception :" + ex.Message + ex.StackTrace);
                throw;
            }
            Logger.Info ("Ended Execution for Processing Deleting Transaction at:" + TenantTime.Now + " EntityType:" + entityType + "for AccountId:" + accountId + " EntityId:" + entityId);
            return result;
        }
        #endregion
    }

}