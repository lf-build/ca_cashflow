﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace CapitalAlliance.Cashflow
{
    public static class CashflowListenerExtensions
    {
        public static void UseCashflowListener(this IApplicationBuilder application)
        {
            application.ApplicationServices.GetRequiredService<ICashflowListener>().Start();
        }
    }
}
