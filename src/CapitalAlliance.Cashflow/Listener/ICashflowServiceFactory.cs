﻿
using LendFoundry.Security.Tokens;

namespace CapitalAlliance.Cashflow
{
    public interface ICashflowServiceFactory
    {
        ICashflowService Create(ITokenReader reader, ITokenHandler handler);
    }
}
