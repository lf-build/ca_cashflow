﻿using System;

namespace CapitalAlliance.Cashflow
{
    public interface IRecoverabilityPolicy
    {
        bool CanBeRecoveredFrom(Exception exception);
    }
}
