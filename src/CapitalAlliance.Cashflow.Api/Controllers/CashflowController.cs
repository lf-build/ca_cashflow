﻿using System.IO;
using System.Threading.Tasks;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using Microsoft.AspNetCore.Mvc;

namespace CapitalAlliance.Cashflow.Api.Controllers {
    [Route ("/")]
    public class CashflowController : ExtendedController {
        public CashflowController (ICashflowService service, ILogger logger) : base (logger) {
            Service = service;
        }

        private ICashflowService Service { get; }

        [HttpPost ("{entitytype}/{entityId}/add/account")]

        [ProducesResponseType (typeof (CapitalAlliance.Cashflow.IAddAccountResponse), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]

        public async Task<IActionResult> AddBankAccount (string entityType, string entityId, [FromBody] BankAccountRequest bankAccount) {
            return Ok (await Task.Run (() => Service.AddBankAccount (entityType, entityId, bankAccount)));
        }

        #region Add Accounts - Bulk
        [HttpPost ("{entitytype}/{entityId}/add/account/bulk")]

        [ProducesResponseType (typeof (CapitalAlliance.Cashflow.IAddAccountBulkResponse), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]

        public async Task<IActionResult> AddBankAccountBulk (string entityType, string entityId, [FromBody] BulkAccountRequest bankAccount) {
            return Ok (await Task.Run (() => Service.AddBankAccountBulk (entityType, entityId, bankAccount)));
        }
        #endregion

        [HttpPost ("{entitytype}/{entityId}/add/transaction")]

        [ProducesResponseType (typeof (string), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]

        public async Task<IActionResult> AddBankTransaction (string entityType, string entityId, [FromBody] BankTransactionRequest bankTransaction) {
            return await ExecuteAsync (async () => {
                await Service.AddBankTransaction (entityType, entityId, bankTransaction);
                return Ok ();
            });
        }

        [HttpPost ("{entitytype}/{entityId}/add/cashflow")]

        [ProducesResponseType (204)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]

        public async Task<IActionResult> AddCashFlowData (string entityType, string entityId, [FromBody] CashflowRequest cashflowRequest) {
            return await ExecuteAsync (
                async () => {
                    await Service.AddCashFlowData (entityType, entityId, cashflowRequest);
                    return Ok ();
                });
        }

        [HttpGet ("/bank/by/{routingNumber}")]

        [ProducesResponseType (typeof (string), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]

        public async Task<IActionResult> GetBankName (string routingNumber) {
            return await ExecuteAsync (async () => {
                return Ok (await Task.Run (() => Service.GetBankName (routingNumber)));
            });
        }

        [HttpPost ("{entitytype}/{entityId}/add/account/preference")]

        [ProducesResponseType (typeof (bool), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]

        public async Task<IActionResult> AddAccountPreference (string entityType, string entityId, [FromBody] AccountPreferenceRequest accountPreference) {
            return Ok (await Task.Run (() => Service.AddAccountPreference (entityType, entityId, accountPreference)));
        }

        [HttpGet ("{entitytype}/{entityId}/{accountId}")]

        [ProducesResponseType (typeof (CapitalAlliance.Cashflow.IBankAccount), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]

        public async Task<IActionResult> GetBankAccountDetails (string entityType, string entityId, string accountId) {
            return await ExecuteAsync (async () => {
                return Ok (await Task.Run (() => Service.GetBankAccountDetails (entityType, entityId, accountId)));
            });
        }

        [HttpPost ("{entitytype}/{entityId}")]

        [ProducesResponseType (typeof (CapitalAlliance.Cashflow.IAccountTypeResponse), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]

        public async Task<IActionResult> GetAccountByType (string entityType, string entityId, [FromBody] AccountTypeRequest request) {
            return await ExecuteAsync (async () => {
                return Ok (await Task.Run (() => Service.GetAccountByType (entityType, entityId, request)));
            });
        }

        [HttpPost ("{entitytype}/{entityId}/add/transaction/bulk")]

        [ProducesResponseType (typeof (string), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]

        public async Task<IActionResult> AddBankTransactionBulk (string entityType, string entityId, [FromBody] BulkTransactionRequest bankTransaction) {
            return await ExecuteAsync (async () => {
                await Service.AddBankTransaction (entityType, entityId, bankTransaction);
                return Ok ();
            });
        }

        [HttpPost ("{entitytype}/{entityId}/calculate/runningbalance")]

        [ProducesResponseType (typeof (bool), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]

        public async Task<IActionResult> CalculateRunningBalance (string entityType, string entityId, [FromBody] object data) {
            return await ExecuteAsync (async () => {
                await Service.CalculateRunningBalance (entityType, entityId, data);
                return Ok ();
            });
        }

        [HttpGet ("{entitytype}/{entityId}/calculate/runningbalance/all")]

        [ProducesResponseType (typeof (bool), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]

        public async Task<IActionResult> CalculateRunningBalanceAll (string entityType, string entityId) {
            return await ExecuteAsync (async () => {
                await Service.CalculateRunningBalanceAll (entityType, entityId);
                return Ok ();
            });
        }

        [HttpGet ("{entitytype}/{entityId}/transaction/{accountId}")]

        [ProducesResponseType (typeof (CapitalAlliance.Cashflow.ITransactionResponse), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]

        public async Task<IActionResult> GetAccountTransaction (string entityType, string entityId, string accountId) {
            return await ExecuteAsync (async () => {
                return Ok (await Task.Run (() => Service.GetAccountTransaction (entityType, entityId, accountId)));
            });
        }

        [HttpGet ("{entitytype}/{entityId}/report/{accountNumber}/{reportType}/{month?}/{year?}")]

        [ProducesResponseType (typeof (string), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]

        public async Task<IActionResult> GenerateAndSaveSingleCashflowReport (string entityType, string entityId, string accountNumber, CashflowReportType reportType, string month = null, string year = null) {
            return await ExecuteAsync (async () => {
                return Ok (await Task.Run (() => Service.GenerateAndSaveSingleCashflowReport (entityType, entityId, accountNumber, reportType, month, year)));
            });
        }

        [HttpDelete ("{entitytype}/{entityId}/delete/transaction/by/account/{accountId}")]

        [ProducesResponseType (typeof (string), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]

        public async Task<IActionResult> DeleteTransactionByAccountId (string entityType, string entityId, string accountId) {
            return await ExecuteAsync (async () => {
                await Service.DeleteTransactionByAccountId (entityType, entityId, accountId);
                return Ok ();
            });
        }

        [HttpGet ("{entitytype}/{entityId}/verify/transaction/by/account/{accountId}/{totalFetchedTransaction}")]

        [ProducesResponseType (typeof (bool), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]
        public async Task<IActionResult> VerifyTransactionsCount (string entityType, string entityId, string accountId, int totalFetchedTransaction) {
            return await ExecuteAsync (async () => {
                return Ok (await Task.Run (() => Service.VerifyTransactionsCount (entityType, entityId, accountId, totalFetchedTransaction)));
            });
        }
    }
}