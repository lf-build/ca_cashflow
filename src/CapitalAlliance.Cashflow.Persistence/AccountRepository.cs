﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using System.Threading.Tasks;
using System.Linq;
using LendFoundry.Foundation.Services;
using System.Collections.Generic;

namespace CapitalAlliance.Cashflow.Persistence
{
    public class AccountRepository : MongoRepository<IBankAccount, BankAccount>, IAccountRepository
    {
        static AccountRepository()
        {
            BsonClassMap.RegisterClassMap<BankAccount>(map =>
            {
                map.AutoMap();
                var type = typeof(BankAccount);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
            BsonClassMap.RegisterClassMap<TimeBucket>(map =>
            {
                map.AutoMap();
                map.MapMember(m => m.Time).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                var type = typeof(TimeBucket);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(false);
            });
        }

        public AccountRepository(ITenantService tenantService, IMongoConfiguration configuration)
           : base(tenantService, configuration, "BankAccount")
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof(TimeBucket)))
            {
                BsonClassMap.RegisterClassMap<TimeBucket>(map =>
                {
                    map.AutoMap();
                    map.MapMember(m => m.Time).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                    var type = typeof(TimeBucket);
                    map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                    map.SetIsRootClass(false);
                });
            }

            CreateIndexIfNotExists("BankAccount_BankAccountId", Builders<IBankAccount>.IndexKeys.Ascending(i => i.EntityId));
            CreateIndexIfNotExists("tenant", Builders<IBankAccount>.IndexKeys.Ascending(i => i.TenantId));
        }

        public async Task<string> AddBankAccount(IBankAccount request)
        {
            if (!string.IsNullOrEmpty(request.Id))
            {
                var account = await Get(request.Id);
                if (account != null)
                {
                    account.RoutingNumber = request.RoutingNumber;
                    account.BankName = request.BankName;
                    account.AccountNumber = request.AccountNumber;
                    account.AccountType = request.AccountType;
                    account.AvailableBalance = request.AvailableBalance;
                    account.CurrentBalance = request.CurrentBalance;
                    account.UpdatedOn = request.UpdatedOn;
                    account.UpdatedBy = request.UpdatedBy;
                    account.BalanceAsOfDate = request.BalanceAsOfDate;
                    account.NameOnAccount = request.NameOnAccount;
                    account.OfficialAccountName = request.OfficialAccountName;
                    account.AccessToken = request.AccessToken;
                    Update(account);
                }
            }
            else
            {
                Add(request);
                return request.Id;
            }
            return request.Id;
        }

        public async Task<bool> ResetAccountPreference(string entityType, string entityId, AccountType accountType)
        {
            var records = Query.Where(i => i.EntityId == entityId && i.EntityType == entityType);
            if (records == null || !records.Any())
                throw new NotFoundException($"Record with entityId {entityId} is not found");
            if (accountType == AccountType.Cashflow)
            {
                foreach (var account in records)
                {
                    account.IsCashflowAccount = false;
                    Update(account);
                }
            }
            else if (accountType == AccountType.Funding)
            {
                foreach (var account in records)
                {
                    account.IsFundingAccount = false;
                    Update(account);
                }
            }
            return true;
        }

        public async Task<bool> UpdateAccountPreference(IAccountPreference request)
        {
            var account = await Get(request.AccountID);

            if (account == null)
            {
                return false;
            }
            if (request.IsCashflowAccount.HasValue)
            {
                account.IsCashflowAccount = request.IsCashflowAccount.Value;
            }
            if (request.IsFundingAccount.HasValue)
            {
                account.IsFundingAccount = request.IsFundingAccount.Value;
            }

            Update(account);

            return true;
        }
        public async Task<IBankAccount> GetAccountDetails(string entityType, string entityId, string id)
        {
            var record = Query.Where(i => i.Id == id && i.EntityId == entityId && i.EntityType == entityType);
            if (record == null || !record.Any())
                throw new NotFoundException($"Record with reference number {id} is not found");

            return record.FirstOrDefault();
        }
        public async Task<List<IBankAccount>> GetAllBankAccounts(string entityType, string entityId)
        {
            var record = Query.Where(i => i.EntityId == entityId && i.EntityType == entityType);
            if (record == null || !record.Any())
                throw new NotFoundException($"Record with entityId {entityId} is not found");

            return record.ToList();
        }

        public async Task<IBankAccount> GetCashflowAccount(string entityType, string entityId)
        {
            var record = Query.Where(i => i.EntityId == entityId && i.EntityType == entityType && i.IsCashflowAccount == true);
            if (record == null || !record.Any())
                throw new NotFoundException($"Record with entityId {entityId} is not found");

            return record.FirstOrDefault();
        }
        public async Task<IBankAccount> GetFundingAccount(string entityType, string entityId)
        {
            var record = Query.Where(i => i.EntityId == entityId && i.EntityType == entityType && i.IsFundingAccount == true);
            if (record == null || !record.Any())
                throw new NotFoundException($"Record with entityId {entityId} is not found");

            return record.FirstOrDefault();
        }

        public async Task<List<IBankAccount>> AddBankAccount(string entityType,string entityId, List<IBankAccount> request)
        {
            var tenantId = TenantService.Current.Id;
            request.ForEach(t => t.TenantId = tenantId);
            Collection.InsertMany(request);
            return await GetAllBankAccounts(entityType, entityId);
        }
    }
}