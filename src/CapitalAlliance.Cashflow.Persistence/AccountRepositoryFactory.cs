﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using System;
using Microsoft.Extensions.DependencyInjection;

namespace CapitalAlliance.Cashflow.Persistence
{
    public class AccountRepositoryFactory : IAccountRepositoryFactory
    {
        public AccountRepositoryFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public IAccountRepository Create(ITokenReader reader)
        {
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);
            var mongoConfiguration = Provider.GetService<IMongoConfiguration>();

            return new AccountRepository(tenantService, mongoConfiguration);
        }       
    }
}