﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using System;
using Microsoft.Extensions.DependencyInjection;

namespace CapitalAlliance.Cashflow.Persistence
{
    public class TransactionRepositoryFactory : ITransactionRepositoryFactory
    {
        public TransactionRepositoryFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public ITransactionRepository Create(ITokenReader reader)
        {
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);
            var mongoConfiguration = Provider.GetService<IMongoConfiguration>();

            return new TransactionRepository(tenantService, mongoConfiguration);
        }       
    }
}