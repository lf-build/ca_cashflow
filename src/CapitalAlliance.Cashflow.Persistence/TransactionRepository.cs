﻿using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;

namespace CapitalAlliance.Cashflow.Persistence {
    public class TransactionRepository : MongoRepository<ITransaction, Transaction>, ITransactionRepository {
        static TransactionRepository () {
            BsonClassMap.RegisterClassMap<Transaction> (map => {
                map.AutoMap ();
                var type = typeof (Transaction);
                map.SetDiscriminator ($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass (true);
            });
        }

        public TransactionRepository (ITenantService tenantService, IMongoConfiguration configuration) : base (tenantService, configuration, "BankTransaction") {
            CreateIndexIfNotExists ("BankTransaction_BankTransactionId", Builders<ITransaction>.IndexKeys.Ascending (i => i.EntityId));
            CreateIndexIfNotExists ("AccountId_1_TenantId_1_TransactionOn.Time.DateTime_-1",
                Builders<ITransaction>.IndexKeys
                .Ascending (i => i.TenantId)
                .Ascending (i => i.AccountId)
                .Descending (i => i.TransactionOn.Time.DateTime));
        }

        public async Task<string> AddTransaction (ITransaction request) {
            if (!string.IsNullOrEmpty (request.Id)) {
                var Transaction = await Get (request.Id);
                if (Transaction != null) {
                    Transaction.AccountId = request.AccountId;
                    Transaction.Amount = request.Amount;
                    Transaction.Categories = request.Categories;
                    Transaction.CategoryId = request.CategoryId;
                    Transaction.Description = request.Description;
                    Transaction.Meta = request.Meta;
                    Transaction.Pending = request.Pending;
                    Transaction.ProviderAccountId = request.ProviderAccountId;
                    Transaction.TransactionDate = request.TransactionDate;
                    Transaction.UpdatedOn = request.UpdatedOn;
                    Update (request);
                }
            } else {
                Add (request);
                return request.Id;
            }
            return request.Id;
        }

        public async Task<List<ITransaction>> GetAllBankTransaction (string entityType, string entityId, string accountId) {
            var record = Query.Where (i => i.EntityId == entityId && i.EntityType == entityType && i.AccountId == accountId);
            return record?.ToList ();
        }
        public async Task<int> GetAllBankTransactionCount (string entityType, string entityId, string accountId) {
            var record = Query.Where (i => i.EntityId == entityId && i.EntityType == entityType && i.AccountId == accountId).Count();
            return record;
        }

        public async Task<string> AddTransaction (List<ITransaction> request) {
            var tenantId = TenantService.Current.Id;
            request.ForEach (t => t.TenantId = tenantId);
            Collection.InsertMany (request);
            return "Success";
        }

        public async Task<string> DeleteTransactionByAccountId (string entityType, string entityId, string accountId) {
            var tenantId = TenantService.Current.Id;
            await Collection.DeleteManyAsync (Builders<ITransaction>.Filter
                .Where (transaction => transaction.TenantId == tenantId &&
                    transaction.EntityId == entityId &&
                    transaction.AccountId == accountId

                ));
            return "Success";
        }
    }
}